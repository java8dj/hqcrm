<?php
namespace app\crm\controller;

use app\common\controller\StoreBase;
use app\crm\model\CrmCustomer;
use app\crm\model\CrmTrack;
use app\store\model\Notice;

class Track extends StoreBase
{
    /**
     * 用户列表
     * @return mixed
     */
    public function index()
    {
        return $this->fetch('index');
    }
    public function index_json($limit='')
    {
        $model = new CrmTrack();
        //读取我的客户
        $customerid = CrmCustomer::where('user_id','in', my_auth())->column('id');
        $list = $model->with(['user','customer'])->where('customer_id','in', $customerid)->order('create_time desc,id desc')->paginate($limit);
        $this->result($list);
    }

    /**
     * 新增用户
     * @return mixed
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $param = $this->request->param();
            $param['store_id'] = StoreId();
            $param['user_id'] = UserId();
            $result = CrmTrack::create($param);
            if ($result == true) {
                if(isset($param['is_notice']) && $param['is_notice'] == 'on'){
                    Notice::insert_notice($param['customer_id'],$param['notice_datetime'],'track',$param['notice_remark']);
                }
                CrmCustomer::where('id',$param['customer_id'])->update(['update_time'=>time()]);
                insert_user_log('添加了客户跟踪信息');
                $this->success('添加成功');
            } else {
                $this->error($this->errorMsg);
            }
        }
        $customer = CrmCustomer::get_list();
        return $this->fetch('save', [
            'customer'     => $customer,
        ]);
    }

    /**
     * 修改用户
     * @param $id 编辑
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function edit($id)
    {
        if ($this->request->isPost()) {
            $param = $this->request->param();
            //更新数据
            $resule = CrmTrack::update($param,['id'=>$param['id']]);
            if ( $resule == true) {
                insert_user_log('修改了客户跟踪信息');
                $this->success('修改成功');
            } else {
                $this->error($this->errorMsg);
            }
        }
        $data = CrmTrack::where('id', $id)->find()?:$this->error('参数错误');
        $customer = CrmCustomer::get_list();
        return $this->fetch('save', [
            'data'      => $data,
            'customer'  => $customer,
        ]);
    }

    /**
     * 删除用户
     */
    public function del()
    {
        if ($this->request->isPost()) {
            $param = $this->request->param();
            CrmTrack::destroy($param['id']);
            insert_user_log('删除客户跟进信息');
            $this->success('删除成功');
        }
    }

    
}