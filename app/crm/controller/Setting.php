<?php
namespace app\crm\controller;
use app\common\controller\StoreBase;

use app\crm\model\CrmSetting;
use app\crm\model\CrmField;

class Setting extends StoreBase
{
    /**
     * CRM配置
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function crm()
    {
        if ($this->request->isPost()) {
            $param = $this->request->param();
            CrmSetting::where('name','crm')->cache('setting_crm'.StoreId(),864000,'store'.StoreId())->update(['jdata'=>$param]);
            $this->success('设置成功');
        }
        //读取初始化字段
        CrmSetting::set_initialize('crm');
        $data = CrmSetting::where('name','crm')->find()['jdata'];
        return $this->fetch('crm', ['data' => $data]);
    }
    /**
     * 客户字段设置
     * @return mixed
     */
    public function field()
    {
        if ($this->request->isPost()) {
            $param = $this->request->param();
            foreach ($param['field'] as $i=>$r){
                if(isset($r['is_status']) && $r['is_status'] == 'on'){
                    $r['is_status'] = 1;
                }else{
                    $r['is_status'] = 0;
                }
                if(isset($r['is_show']) && $r['is_show'] == 'on'){
                    $r['is_show'] = 1;
                }else{
                    $r['is_show'] = 0;
                }
                if(isset($r['is_required']) && $r['is_required'] == 'on'){
                    $r['is_required'] = 1;
                }else{
                    $r['is_required'] = 0;
                }
                CrmField::where('id',$r['id'])->update($r);
            }
            cache('customer_field'.StoreId(),null);
            //CrmField::saveAll($param['field']);
            $this->success('设置成功');
        }
        //读取初始化字段
        CrmField::set_initialize();
        $field = CrmField::get_list();

        return $this->fetch('field', [
            'field'          => $field
        ]);
    }
    
}
