<?php
namespace app\crm\controller;

use app\common\controller\StoreBase;
use app\crm\model\CrmCustomer;
use app\crm\model\CrmContract;
use app\store\model\Notice;
use app\store\model\User;

class Contract extends StoreBase
{
    /**
     * 用户列表
     * @return mixed
     */
    public function index()
    {
        return $this->fetch('index');
    }
    public function index_json($limit='')
    {
        $model = new CrmContract();
        $list = $model->with(['user','customer'])->order('create_time desc,id desc')->paginate($limit);
        $this->result($list);
    }

    /**
     * 新增用户
     * @return mixed
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $param = $this->request->param();
            $param['store_id'] = StoreId();
            $param['user_id'] = UserId();
            $verify = input('_verify', true);
            if($verify!='0'){
                try{
                    $this->validate($param, 'contract');
                }catch (\Exception $e){
                    $this->error($e->getMessage());
                }
            }
            $result = CrmContract::create($param);
            if ($result == true) {
                Notice::insert_notice($param['customer_id'],$param['end_time'],'contract','编号:'.$param['contract_no'].'的合同到期。请及时安排续约事宜');
                CrmCustomer::where('id',$param['customer_id'])->update(['update_time'=>time()]);
                insert_user_log('添加了客户合同');
                $this->success('添加成功');
            } else {
                $this->error($this->errorMsg);
            }
        }
        $customer = CrmCustomer::get_list();
        return $this->fetch('save', [
            'customer'     => $customer,
            'user'         => User::get_list(),
            'product'      => get_dict_list(16)
        ]);
    }

    /**
     * 修改用户
     * @param $id 编辑
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function edit($id)
    {
        if ($this->request->isPost()) {
            $param = $this->request->param();
            //更新数据
            $resule = CrmContract::update($param,['id'=>$param['id']]);
            if ( $resule == true) {
                insert_user_log('修改了客户合同');
                $this->success('修改成功');
            } else {
                $this->error($this->errorMsg);
            }
        }
        $data = CrmContract::where('id', $id)->find()?:$this->error('参数错误');
        $customer = CrmCustomer::get_list();
        return $this->fetch('save', [
            'data'      => $data,
            'customer'     => $customer,
            'user'         => User::get_list(),
            'product'      => get_dict_list(16)
        ]);
    }

    /**
     * 删除用户
     */
    public function del()
    {
        if ($this->request->isPost()) {
            $param = $this->request->param();
            CrmContract::destroy($param['id']);
            insert_user_log('删除了客户合同');
            $this->success('删除成功');
        }
    }

    
}