<?php
namespace app\crm\validate;
use think\Validate;

class Contract extends Validate
{
    protected $rule = [
        'contract_no'   => 'require|unique:crm_contract',
    ];

    protected $message = [
        'contract_no.require'    => '合同编号不能为空',
        'contract_no.unique'    => '合同编号已存在',
    ];
}
