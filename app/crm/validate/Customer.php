<?php
namespace app\crm\validate;
use think\Validate;

class Customer extends Validate
{
    protected $rule = [
        'name'     => 'require',
        'mobile'   => 'require|mobile|unique:user',
        'rule_id'  => 'require'
    ];

    protected $message = [
        'name.require'     => '部门名称不能为空',
        'mobile.require'   => '手机号码不能为空',
        'mobile.mobile'    => '手机号码格式错误',
        'mobile.unique'    => '手机号码已存在',
        'rule_id.require'  => '角色组不能为空',
    ];
}
