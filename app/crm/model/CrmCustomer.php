<?php
namespace app\crm\model;
use think\Model;
use think\model\concern\SoftDelete;
use app\store\model\DictData;
use app\store\model\User;

class CrmCustomer extends Model
{
    use SoftDelete;
    protected $deleteTime = 'delete_time';
    protected $defaultSoftDelete = 0;

    protected $globalScope = ['store_id'];
    public function scopeStore_id($query)
    {
        $query->where('store_id', StoreId())->where('user_id','in', my_auth());
    }

    protected $type = [
        'birthday'     => 'timestamp:Y-m-d',
        'draw_time'     => 'timestamp:Y-m-d',
    ];

    /**
     * 关联部门表
     * @return \think\model\relation\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * 关联商户表
     * @return \think\model\relation\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(DictData::class, 'category_id', 'id')->bind(['category'=>'name']);
    }
    /**
     * 关联商户表
     * @return \think\model\relation\BelongsTo
     */
    public function from()
    {
        return $this->belongsTo(DictData::class, 'from_id', 'id')->bind(['from'=>'name']);
    }
    /**
     * 关联商户表
     * @return \think\model\relation\BelongsTo
     */
    public function industry()
    {
        return $this->belongsTo(DictData::class, 'industry_id', 'id')->bind(['industry'=>'name']);
    }

    public function track()
    {
        return $this->hasMany(CrmTrack::class,'customer_id','id');
    }
    public function contract()
    {
        return $this->hasMany(CrmContract::class,'customer_id','id');
    }

    public function setProvinceAttr($value,$data)
    {
        if(!$data['shengshi']){
            $list = array();
        }else{
            $list = explode(',',$data['shengshi']);
        }
        return $list[0];
    }

    public function setCityAttr($value,$data)
    {
        if(!$data['shengshi']){
            $list = array();
        }else{
            $list = explode(',',$data['shengshi']);
        }
        return $list[1];
    }

    public function setRegionAttr($value,$data)
    {
        if(!$data['shengshi']){
            $list = array();
        }else{
            $list = explode(',',$data['shengshi']);
        }
        return $list[2];
    }
    /**
     * 获取列表
     * @return \think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    static function get_list() {
        return self::order('sort_order desc,id desc')->select();
    }

    /**
     * 客户状态
     * @return array[]
     */
    static function get_status()
    {
        $data = [
            ['id' => 0, 'name' => '成交客户'],
            ['id' => 1, 'name' => '目标客户'],
            ['id' => 2, 'name' => '潜在客户'],
            ['id' => 3, 'name' => '流失客户'],
            ['id' => 4, 'name' => '非客户'],
        ];
        return $data;
    }

    /**
     *
     * @param $value
     * @param $data
     * @return string
     */
    public function getStatusAttr($value,$data)
    {
        $status = self::get_status();
        return $status[$data['status']]['name'];
    }




}