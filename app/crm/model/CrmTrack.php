<?php
namespace app\crm\model;
use think\Model;
use think\model\concern\SoftDelete;
use app\store\model\User;

class CrmTrack extends Model
{
    use SoftDelete;
    protected $deleteTime = 'delete_time';
    protected $defaultSoftDelete = 0;

    protected $globalScope = ['store_id'];
    public function scopeStore_id($query)
    {
        $query->where('store_id', StoreId())->where('customer_id','in', CrmCustomer::column('id'));
    }

    /**
     * 关联客户
     * @return \think\model\relation\BelongsTo
     */
    public function customer()
    {
        return $this->belongsTo(CrmCustomer::class,'customer_id','id');
    }

    /**
     * 关联用户
     * @return \think\model\relation\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }
    /**
     * 添加跟进记录
     * @param $customer_id 客户ID
     * @param $type 类型
     * @param $remark 跟踪内容
     * @return array|Model|null
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    static function insert_system_track($customer_id,$type='0',$remark)
    {
        $customer = explode(',', $customer_id);
        //判断是否多客户
        if(is_array($customer) == true){
            foreach ($customer as $r){
                self::create([
                    'store_id'    => StoreId(),
                    'customer_id' => $r,
                    'user_id'     => UserId(),
                    'type'        => $type,
                    'remark'      => $remark,
                ]);
            }
        }else{
            self::create([
                'store_id'    => StoreId(),
                'customer_id' => $customer_id,
                'user_id'     => UserId(),
                'type'        => $type,
                'remark'      => $remark,
            ]);
        }

        return ;
    }




}