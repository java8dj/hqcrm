<?php
namespace app\crm\model;
use think\Model;
use think\model\concern\SoftDelete;

class CrmSetting extends Model
{
    protected $json = ['jdata'];
    protected $jsonAssoc = true;

    protected $globalScope = ['store_id'];
    public function scopeStore_id($query)
    {
        $query->where('store_id', StoreId());
    }

    /**
     * 初始化配置
     */
    static function set_initialize($name)
    {
        //判断是否有
        $data = self::where('name',$name)->find();
        if(empty($data)){
            self::create([
                'store_id' => StoreId(),
                'name'     => $name,
            ]);
            $data = self::where('name',$name)->find();
        }
        return $data;
    }




}