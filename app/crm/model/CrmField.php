<?php
namespace app\crm\model;
use think\Model;

class CrmField extends Model
{
    protected $globalScope = ['store_id'];
    public function scopeStore_id($query)
    {
        $query->where('store_id', StoreId());
    }

    /**
     * 初始化配置
     */
    static function set_initialize()
    {
        $list = [
            ['field'=>'company',  'name'=>'公司','type'=>'text',    'is_status'=>1,'is_show'=>1,'is_required'=>1,'sort_order'=>100],
            ['field'=>'name',     'name'=>'姓名','type'=>'text',    'is_status'=>1,'is_show'=>1,'is_required'=>1,'sort_order'=>100],
            ['field'=>'image',    'name'=>'图片','type'=>'image',   'is_status'=>1,'is_show'=>1,'is_required'=>1,'sort_order'=>100],
            ['field'=>'mobile',   'name'=>'手机','type'=>'text',    'is_status'=>1,'is_show'=>1,'is_required'=>1,'sort_order'=>100],
            ['field'=>'wechat',   'name'=>'微信','type'=>'text',    'is_status'=>1,'is_show'=>1,'is_required'=>1,'sort_order'=>100],
            ['field'=>'email',    'name'=>'邮箱','type'=>'text',    'is_status'=>1,'is_show'=>1,'is_required'=>1,'sort_order'=>100],
            ['field'=>'phone',    'name'=>'电话','type'=>'text',    'is_status'=>1,'is_show'=>1,'is_required'=>1,'sort_order'=>100],
            ['field'=>'address',  'name'=>'地址','type'=>'address', 'is_status'=>1,'is_show'=>1,'is_required'=>1,'sort_order'=>100],
            ['field'=>'birthday', 'name'=>'生日','type'=>'date',    'is_status'=>1,'is_show'=>1,'is_required'=>1,'sort_order'=>100],
            ['field'=>'category', 'name'=>'分类','type'=>'category','is_status'=>1,'is_show'=>1,'is_required'=>1,'sort_order'=>100],
            ['field'=>'from',     'name'=>'来源','type'=>'from',    'is_status'=>1,'is_show'=>1,'is_required'=>1,'sort_order'=>100],
            ['field'=>'industry', 'name'=>'行业','type'=>'industry','is_status'=>1,'is_show'=>1,'is_required'=>1,'sort_order'=>100],
            ['field'=>'tag',      'name'=>'标签','type'=>'tag',     'is_status'=>1,'is_show'=>1,'is_required'=>1,'sort_order'=>100],
            ['field'=>'status',   'name'=>'状态','type'=>'status',  'is_status'=>1,'is_show'=>1,'is_required'=>1,'sort_order'=>100],
            ['field'=>'remark',   'name'=>'备注','type'=>'text',    'is_status'=>1,'is_show'=>1,'is_required'=>1,'sort_order'=>100],
        ];
        //查看是否添加
        foreach ($list as $k=>$r){
            $data = self::where('field',$r['field'])->find();
            if(empty($data)){
                self::create([
                    'store_id'   => StoreId(),
                    'field'      => $r['field'],
                    'type'       => $r['type'],
                    'name'       => $r['name'],
                    'is_status'  => $r['is_status'],
                    'is_show'    => $r['is_show'],
                    'is_required'=> $r['is_required'],
                    'sort_order' => $r['sort_order'],
                ]);
            }
        }
        return $data;
    }
    /**
     * 获取列表
     * @return \think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    static function get_list()
    {
        return self::order('sort_order desc,id asc')->cache('customer_field'.StoreId(),86400)->select();
    }

}