<?php
namespace app\wechat\model;
use think\Model;
use EasyWeChat\Factory;
use think\model\concern\SoftDelete;

class WechatReply extends Model
{
    use SoftDelete;
    protected $deleteTime = 'delete_time';
    protected $defaultSoftDelete = 0;
    protected $autoWriteTimestamp = true;

    protected $globalScope = ['shop_id'];
    public function scopeStore_id($query)
    {
        $query->where('shop_id', StoreId());
    }
    //列出所有关键字
    public static function get_lists()
    {
        $list = self::where('status',1)->select();
        return $list;
    }
    
}