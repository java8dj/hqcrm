<?php
namespace app\wechat\model;
use think\Model;
use EasyWeChat\Factory;
use think\facade\Request;
use think\facade\Db;
use think\facade\Cache;
use think\facade\Session;
use think\facade\Config;
use think\model\concern\SoftDelete;

class WechatMessage extends Model
{
    use SoftDelete;
    protected $deleteTime = 'delete_time';
    protected $defaultSoftDelete = 0;
    protected $autoWriteTimestamp = true;
    protected $globalScope = ['shop_id'];
    public function scopeShop_id($query) 
    {
        $query->where('shop_id', ShopId());
    }
    
    public function wxuser()
    {
        return $this->belongsTo(WechatUser::class, 'openid', 'openid');
    }
    
    
}