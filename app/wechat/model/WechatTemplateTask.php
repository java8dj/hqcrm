<?php
namespace app\wechat\model;
use think\Model;

class WechatTemplateTask extends Model
{
    protected $globalScope = ['store_id'];
    public function scopeStore_id($query)
    {
        $query->where('store_id', StoreId());
    }
    public function user()
    {
        return $this->belongsTo(WechatUser::class,'user_id','id')->bind(['openid','nickname']);
    }

}