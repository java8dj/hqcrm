<?php
namespace app\wechat\model;
use think\Model;
use EasyWeChat\Factory;

use app\user\model\WechatReply;
use app\user\model\WechatNews;

class WechatMenu extends Model
{
    protected $autoWriteTimestamp = true;

    protected $globalScope = ['store_id'];
    public function scopeStore_id($query)
    {
        $query->where('store_id', StoreId());
    }
    
    //获取菜单消息
    public static function create_menu($data)
    {
        switch ($data['type']){
            //文本
            case 'text':
                //将内容保存到数据库.并设定一个KEY
                $reply['store_id'] = StoreId();
                $reply['keyword'] = '公众号菜单回复'.$data['type']. rand_number(5);
                $reply['type'] = 'text';
                $reply['content'] = $data['value'];
                //判断是否已有同样的自定义回复
                $result = WechatReply::where('content',$data['value'])->find();
                if($result){
                    $data['key'] = $result['keyword'];
                }else{
                    WechatReply::create($reply);
                    $data['key'] = $reply['keyword'];
                }
                $data['type'] = 'click';
                break;
            //图片
            case 'img':
                //将内容保存到数据库.并设定一个mediaid
                $reply['store_id'] = StoreId();
                $reply['keyword'] = '公众号菜单回复'.$data['type']. rand_number(5);
                $reply['type'] = 'image';
                $reply['mediaid'] = $data['value'];
                $reply['content'] = $data['value'];
                //判断是否已有同样的自定义回复
                $result = WechatReply::where('mediaid',$data['value'])->find();
                if($result){
                    $data['key'] = $result['keyword'];
                }else{
                    WechatReply::create($reply);
                    $data['key'] = $reply['keyword'];
                }
                $data['type'] = 'click';
                break;
            //图文
            case 'news':
                //判断media_id是否存在
                $news = WechatNews::where('media_id',$data['value'])->find();
                if(!$news){
                    //循环将文章保存
                    foreach($data['news_info']['list'] as  $k=>$r){
                        $news_list['store_id'] = StoreId();
                        $news_list['media_id'] = $data['value'];
                        $news_list['title'] = $r['title'];
                        $news_list['author'] = $r['author'];
                        $news_list['digest'] = $r['digest'];
                        $news_list['show_cover_pic'] = $r['show_cover'];
                        $news_list['thumb_url'] = $r['cover_url'];
                        $news_list['url'] = $r['content_url'];
                        $news_list['content'] = '';
                        $news_list['content_source_url'] = $r['source_url'];
                        $news_list['sort_order'] = $k;
                        WechatNews::create($news_list);
                    }
                }
                //调取自动回复内容
                $result = WechatReply::where(['type'=>'news','mediaid'=>$data['value']])->find();
                if($result){//如果有
                    $data['key'] = $result['keyword'];
                }else{//如果没有
                    $reply['store_id'] = StoreId();
                    $reply['keyword'] = '公众号菜单回复'.$data['type']. rand_number(5);
                    $reply['type'] = 'news';
                    $reply['mediaid'] = $data['value'];
                    $reply['content'] = $data['value'];
                    WechatReply::create($reply);
                    $data['key'] = $reply['keyword'];
                }
                $data['type'] = 'click';
                break;
            //语音
            case 'voice':
                '';
                break;
            //视频
            case 'video':
                '';
                break;
            //跳转网页
            case 'view':
                '';
                break;
            //点击链接
            case 'click':
                '';
                break;
            
            //其他
            default:
                '';
        }
        //保存到数据库
        self::create($data);
        return;
    }
    
    //保存消息
    public static function update_menu($pid)
    {
        //查询上级是否还有子栏目
        $count_menu = self::where('pid',$pid)->count();
        //判断
        if($count_menu == '0'){
            //修改一级栏目
            self::where('id',$pid)->update(['sub_button'=>'0']);
        }else{
            //修改一级栏目
            self::where('id',$pid)->update(['sub_button'=>'1']);
        }
        return '';
    }
    
  
    
}