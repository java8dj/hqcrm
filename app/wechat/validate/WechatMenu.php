<?php

namespace app\wechat\validate;
use think\Validate;

class WechatMenu extends Validate
{
    protected $rule = [
        'name'   => 'require',
        //'type' => 'require',
    ];

    protected $message = [
        'name.require'   => '菜单标题不能为空',
        //'type.require'     => '类型不能为空',
        
    ];
}
