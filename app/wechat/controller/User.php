<?php
namespace app\wechat\controller;

use app\common\controller\StoreBase;
use app\store\model\Store as ShopModel;
use app\wechat\model\WechatUser;
use app\common\model\WechatUser as Wuser;

class User extends StoreBase
{
    protected $noAuth = ['get_user','get_userinfo'];
    public function initialize()
    {
        parent::initialize();
        $param = $this->request->param();
        $this->wxapp = wxapi(StoreId(),'weixin');
    }

    /**
     * 会员管理
     * @return mixed
     * @throws \think\db\exception\DbException
     */
    public function index()
    {
        $param = $this->request->param();
        $user = new WechatUser();
        if (isset($param['subscribe'])) {
            $user = $user->where('subscribe',$param['subscribe']);
        }
        if (isset($param['subscribe_scene'])) {
            $user = $user->where('subscribe_scene',$param['subscribe_scene']);
        }
        $list = $user->order('subscribe_time desc,create_time desc,id desc')->paginate();
        return $this->fetch('index',[
            'list' => $list
        ]);
    }
    public function index_json($limit='15')
    {
        $param = $this->request->param();
        $user = new WechatUser();
        if (isset($param['subscribe'])) {
            $user = $user->where('subscribe',$param['subscribe']);
        }
        if (isset($param['subscribe_scene'])) {
            $user = $user->where('subscribe_scene',$param['subscribe_scene']);
        }
        $list = $user->order('subscribe_time desc,create_time desc,id desc')->paginate($limit);
        $this->result($list);
    }

    /**
     * 拉取OPENID列表
     * @param string $page
     * @param string $next_openid
     * @param string $page_count
     */
    public function get_user($page='1',$next_openid='',$page_count='10000')
    {
        //检查是否有分页
        if(isset($page)){
            //获取列表
            $user_list = $this->wxapp->user->list($next_openid);
            //总共更新页数
            $page_total = ceil($user_list['total']/$page_count);
            //保存Openid
            $result = WechatUser::UpdateOpenid($user_list['data']['openid']);
            //$this->result($openid);
            //剩余条数
            $shengyu = $page_total - $page;
            if($shengyu > '0'){
                $page = $page+1;
                //还有未更新的.继续
                $this->success('本次更新'.$result.'条,还剩'.$shengyu.'页',url('get_user',['page'=>$page,'next_openid'=>$user_list['next_openid']]));
            }else{
                //已经结束
                $this->success('本次更新完成'.$result.'条数据,开始进入获取详情',url('/wechat/user/get_userinfo'));
            }
        }else{
            $this->success('即将开始更新数据',url('get_user',['page'=>1]));
        }
    }

    /**
     * 获取用户详情
     * @param string $page
     * @param string $page_num
     */
    public function get_userinfo($page='1',$page_num='100')
    {
        $list = WechatUser::where('is_getinfo',0)->limit($page_num)->select();
        if(count($list) == 0){
            $this->success('更新完成',url('/wechat/user/index'));
        }else{
            foreach ($list as $l=>$o){
                $openids[$l] = $o['openid'];
            }
            $data = $this->wxapp->user->select($openids);
            foreach ($data['user_info_list'] as $k=>$r){
                $insert[$k]['id']              = $list[$k]['id'];
                $insert[$k]['subscribe']       = !empty($r['subscribe'])?$r['subscribe']:'';
                $insert[$k]['is_getinfo']      = 1;
                $insert[$k]['openid']          = !empty($r['openid'])?$r['openid']:'';
                $insert[$k]['nickname']        = !empty($r['nickname'])?$r['nickname']:'';
                $insert[$k]['gender']          = !empty($r['sex'])?$r['sex']:'';
                $insert[$k]['language']        = !empty($r['language'])?$r['language']:'';
                $insert[$k]['city']            = !empty($r['city'])?$r['city']:'';
                $insert[$k]['province']        = !empty($r['province'])?$r['province']:'';
                $insert[$k]['country']         = !empty($r['country'])?$r['country']:'';
                $insert[$k]['avatarurl']       = !empty($r['headimgurl'])?$r['headimgurl']:'';
                $insert[$k]['subscribe_time']  = !empty($r['subscribe_time'])?$r['subscribe_time']:'';
                $insert[$k]['unionid']         = !empty($r['unionid'])?$r['unionid']:'';
                $insert[$k]['subscribe_scene'] = !empty($r['subscribe_scene'])?$r['subscribe_scene']:'';
                //$model = WechatUser::where('openid',$insert[$k]['openid'])->update($insert[$k]);
            }
            $model = new Wuser();
            $model->saveAll($insert);
            $this->success('本次更新完成100条数据',url('/wechat/user/get_userinfo'));
        }

    }

}
