<?php
namespace app\wechat\controller;
use app\common\controller\StoreBase;
use app\store\model\Store as ShopModel;

class Setting extends StoreBase
{
    /**
     * 微信配置
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function index()
    {
        if ($this->request->isPost()) {
            $param = $this->request->param();
            //更新数据
            $result = ShopModel::cache('shop_info'.StoreId(),86400,'store'.StoreId())->update($param,['id'=> StoreId()]);
            if ($result == true) {
                ShopModel::writeCertPemFiles($param['wxpay']['certpem'], $param['wxpay']['keypem']);
                insert_user_log('修改了微信基本信息');
                $this->success('修改成功');
            } else {
                $this->error('本次未修改任何数据');
            }
        }
        $data = ShopModel::where('id', StoreId())->find();
        return $this->fetch('index', ['data' => $data]);
    }
    
}
