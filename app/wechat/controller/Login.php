<?php
namespace app\wechat\controller;
use app\common\controller\IndexBase;
use think\facade\Session;
class Login extends IndexBase
{
    protected function initialize()
    {
        parent::initialize();
        $param = $this->request->param();
        $this->app = wxapi($param['store_id'],'weixin');
    }
    //首页
    public function index()
    {
        //获取微信OPENID
        $app = $this->app;
        //获取用户openid
        $openid = Session::get('openid');
        //如果没有.就去获取
        if(!$openid){
            $response = $app->oauth->scopes(['snsapi_userinfo'])->redirect($this->request->domain(true).'/wechat/login/login/')->send();
        }
        //判断是否有数据
        //$user = WechatUser::where('openid',$openid)->find();
        //如果有.赋值
        $this->result($openid);
        
    }
    //跳转登陆
    public function login()
    {
        $app = $this->app;
        //获取用户信息
        $openid = $app->oauth->user();
        //保存OPENID
        Session::set('openid',$openid->id);
        //跳转回去
        $this->redirect(url('/wechat/login/index/'));
    }
    
    
}
