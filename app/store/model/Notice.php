<?php
namespace app\store\model;
use think\Model;
use think\model\concern\SoftDelete;
use app\crm\model\CrmCustomer;

class Notice extends Model
{
    use SoftDelete;
    protected $deleteTime = 'delete_time';
    protected $defaultSoftDelete = 0;

    protected $globalScope = ['store_id'];
    public function scopeStore_id($query)
    {
        $query->where('store_id', StoreId());
    }

    protected $type = [
        'datetime'     => 'timestamp:Y-m-d',
    ];

    /**
     * 关联部门表
     * @return \think\model\relation\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * 关联客户
     * @return \think\model\relation\BelongsTo
     */
    public function customer()
    {
        return $this->belongsTo(CrmCustomer::class, 'customer_id', 'id');
    }

    /**
     * 获取列表
     * @return \think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    static function get_list() {
        return self::order('sort_order desc,id desc')->select();
    }

    /**
     * 新增通知
     * @param $customer_id 客户ID
     * @param $datetime 提醒时间
     * @param $type 提醒类型
     * @param $remark 提醒内容
     * @param string $status 提醒状态
     */
    static function insert_notice($customer_id,$datetime,$type,$remark,$status='0')
    {
        self::create([
            'store_id'    => StoreId(),
            'customer_id' => $customer_id,
            'user_id'     => UserId(),
            'datetime'    => $datetime,
            'type'        => $type,
            'remark'      => $remark,
            'status'      => $status
        ]);
        return ;
    }

    /**
     * 分类
     */
    static function typelist()
    {
        $data = [
            ['id'=>'track',   'name'=>'跟进/回访提醒'],
            ['id'=>'contract',   'name'=>'合同事项提醒'],
            ['id'=>'birthday',   'name'=>'生日提醒'],
            ['id'=>'other',   'name'=>'其他事项提醒'],
        ];
        return $data;
    }
}