<?php
namespace app\store\model;
use think\Model;

class StoreLog extends Model
{
    protected $globalScope = ['store_id'];
    
    public function scopeStore_id($query)
    {
        $query->where('store_id', StoreId());
    }
    public function getClientAttr($value)
    {
        $status = [1=>'小程序',0=>'电脑',2=>'小程序'];
        return $status[$value];
    }

    /**
     * 关联商户
     * @return \think\model\relation\BelongsTo
     */
    public function store()
    {
        return $this->belongsTo(Store::class,'store_id','id')->bind(['store_name'=>'name']);
    }

    /**
     * 关联用户
     * @return \think\model\relation\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }
    
    
    
}