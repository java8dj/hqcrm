<?php
namespace app\store\model;
use think\Model;

class StoreAuth extends Model
{
    // 获取导航栏
    public static function getNavbar()
    {
        $access = my_rule();
        $where = ['type' => 'nav', 'status' => 1];
        if($access){
            $where = "type='nav' and status = '1' and id in(".$access.")";
        }
        $navs = self::where($where)->order('sort_order asc')->select();
        return collection($navs)->toArray();
    }
}