<?php
namespace app\store\model;
use app\common\model\StoreGroup;
use think\Model;
use think\model\concern\SoftDelete;

class DictData extends Model
{
    use SoftDelete;
    protected $deleteTime = 'delete_time';
    protected $defaultSoftDelete = 0;
    
    protected $globalScope = ['store_id'];
    public function scopeStore_id($query)
    {
        $query->where('store_id', StoreId());
    }

    public function group()
    {
        return $this->belongsTo(StoreGroup::class, 'group_id', 'id')->bind(['rules','group_name'=>'name']);
    }
    public function store()
    {
        return $this->belongsTo(Store::class, 'store_id', 'id');
    }

    /**
     * 根据名称获取ID
     * @param $name
     * @return mixed
     */
    static function get_dictid($name)
    {
        return self::where('name',$name)->value('id')?:'0';
    }
}