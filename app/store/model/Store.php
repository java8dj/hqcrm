<?php
namespace app\store\model;
use think\Model;
use think\model\concern\SoftDelete;

class Store extends Model
{
    use SoftDelete;
    protected $deleteTime = 'delete_time';
    protected $defaultSoftDelete = 0;
    protected $autoWriteTimestamp = true;

    protected $json = ['wechat','wxapp','wxpay'];
    protected $jsonAssoc = true;

    protected $globalScope = ['id'];
    public function scopeId($query)
    {
        $query->where('id', StoreId());
    }

    protected $type = [
        'exp_time'     => 'timestamp:Y-m-d',
    ];

    public function taocan()
    {
        return $this->belongsTo(\app\common\model\Taocan::class, 'taocan', 'id')->bind(['rules','group_name'=>'name']);
    }
    

    public function setProvinceAttr($value,$data)
    {
        if(!$data['shengshi']){
            $list = array();
        }else{
            $list = explode(',',$data['shengshi']);
        }
        return $list[0];
    }
    
    public function setCityAttr($value,$data)
    {
        if(!$data['shengshi']){
            $list = array();
        }else{
            $list = explode(',',$data['shengshi']);
        }
        return $list[1];
    }
    
    public function setAreaAttr($value,$data)
    {
        if(!$data['shengshi']){
            $list = array();
        }else{
            $list = explode(',',$data['shengshi']);
        }
        return $list[2];
    }
    
    /**
     * 写入cert证书文件
     * @param string $cert_pem
     * @param string $key_pem
     * @return bool
     */
    static function writeCertPemFiles($cert_pem = '', $key_pem = '')
    {
        if (empty($cert_pem) || empty($key_pem)) {
            return false;
        }
        // 证书目录
        $filePath = base_path().'/common/library/wechat/cert/' . StoreId() . '/';
        // 目录不存在则自动创建
        if (!is_dir($filePath)) {
            mkdir($filePath, 0777, true);
        }
        // 写入cert.pem文件
        if (!empty($cert_pem)) {
            file_put_contents($filePath . 'cert.pem', $cert_pem);
        }
        // 写入key.pem文件
        if (!empty($key_pem)) {
            file_put_contents($filePath . 'key.pem', $key_pem);
        }
        return true;
    }
    /**
     * 写入验证文件
     * @param string $cert_pem
     * @param string $key_pem
     * @return bool
     */
    static function writeAuthFiles($filename = '')
    {
        if (empty($cert_pem) || empty($key_pem)) {
            return false;
        }
        // 证书目录
        $filePath = public_path();
        // 目录不存在则自动创建
        if (!is_dir($filePath)) {
            mkdir($filePath, 0777, true);
        }
        // 写入cert.pem文件
        if (!empty($cert_pem)) {
            file_put_contents($filePath . 'cert.pem', $cert_pem);
        }
        // 写入key.pem文件
        if (!empty($key_pem)) {
            file_put_contents($filePath . 'key.pem', $key_pem);
        }
        return true;
    }
    
    
}