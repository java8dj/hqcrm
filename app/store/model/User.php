<?php
namespace app\store\model;
use app\common\model\StoreGroup;
use think\Model;
use think\model\concern\SoftDelete;

class User extends Model
{
    use SoftDelete;
    protected $deleteTime = 'delete_time';
    protected $defaultSoftDelete = 0;

    protected $globalScope = ['store_id'];
    public function scopeStore_id($query)
    {
        $query->where('store_id', StoreId());
    }

    protected $type = [
        'last_login_time'     => 'timestamp:Y-m-d H:i:s',
    ];

    /**
     * 设置密码MD5
     * @param $value
     * @return string
     */
    public function setPasswordAttr($value)
    {
        return md5($value);
    }

    /**
     * 关联部门表
     * @return \think\model\relation\BelongsTo
     */
    public function group()
    {
        return $this->belongsTo(StoreGroup::class, 'group_id', 'id')->bind(['rules','group_name'=>'name','pid']);
    }

    /**
     * 关联商户表
     * @return \think\model\relation\BelongsTo
     */
    public function store()
    {
        return $this->belongsTo(Store::class, 'store_id', 'id');
    }

    /**
     * 获取/设置角色ID
     * @param $value
     * @return array|string
     */
    public function setRuleIdAttr($value)
    {
        if(!$value){
            $list = array();
        }else{
            $list = implode(',',$value);
        }
        return $list;
    }
    public function getRuleIdAttr($value)
    {
        if(!$value){
            $list = array();
        }else{
            $list = explode(',', $value);
        }
        return $list;
    }

    /**
     * 获取列表
     * @return \think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    static function get_list() {
        return self::where('id','in', my_auth())->where('status','1')->order('sort_order desc,id desc')->select();
    }

    /**
     * 获取指定用户的姓名
     * @param $user_id 用户ID
     * @return mixed
     */
    static function get_username($user_id)
    {
        return self::where('id',$user_id)->value('name');
    }
}