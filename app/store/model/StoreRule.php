<?php
namespace app\store\model;
use think\Model;
use think\model\concern\SoftDelete;

class StoreRule extends Model
{
    use SoftDelete;
    protected $deleteTime = 'delete_time';
    protected $defaultSoftDelete = 0;

    protected $globalScope = ['store_id'];
    public function scopeStore_id($query)
    {
        $query->where('store_id', StoreId());
    }
    
    //获取权限组列表
    static function list_to_level() {
        $list = self::order('id desc')->select();
        return $list;
    }
    
    //获取我的权限
    static function my_auth() {
        //查询我的分组
        $user = User::where('id', UserId())->find();
        $auth = self::where('id','in',$user['rule_id'])->value('rules');
        if(!$auth){
            $auth = [];
        }
        return $auth;
    }
}