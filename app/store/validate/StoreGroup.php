<?php
namespace app\store\validate;
use think\Validate;

class StoreGroup extends Validate
{
    protected $rule = [
        'name' => 'require',
    ];

    protected $message = [
        'name.require' => '部门名称不能为空',
    ];
}
