<?php
namespace app\store\validate;
use think\Validate;

class StoreRule extends Validate
{
    protected $rule = [
        'name' => 'require',
        'rules' => 'require',
    ];

    protected $message = [
        'name.require' => '角色名称不能为空',
        'rules.require' => '角色权限不能为空',
    ];
}
