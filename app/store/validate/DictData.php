<?php
namespace app\store\validate;
use think\Validate;

class DictData extends Validate
{
    protected $rule = [
       // 'dict_id' => 'require',
        'name' => 'require',
    ];

    protected $message = [
        //'dict_id.require' => '字典分类不能为空',
        'name.require' => '字典名称不能为空',
    ];
}
