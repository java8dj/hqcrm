<?php
namespace app\store\controller;

use app\common\controller\StoreBase;
use think\facade\View;
use app\common\model\Taocan;
use app\store\model\StoreGroup;
use app\store\model\StoreAuth;
use app\store\model\User;
use app\store\model\Store;

class Group extends StoreBase
{
    /**
     * 部门列表
     * @return mixed
     */
    public function index()
    {
        return $this->fetch('index');
    }
    public function index_json()
    {
        $list = StoreGroup::get_list();
        $authRule = collection($list)->toArray();
        foreach ($authRule as $k => $v) {
            $data[$k] = array(
                  'id'          => $v['id'],
                  "name"        => $v['name'],
                  "description" => $v['description'],
                  "parentId"    => $v['pid'],
                  "isMenu"      => $v['id']=='nav'?0:1,
                  "sort_order"  => $v['sort_order'],
                  "status"      => $v['status'],
                  "createTime"  => '',
                  "updateTime"  => '',
                  "open"        => $v['status']?true:false
                );
        }
        $arrayName = array('code' =>0,'msg' =>'加载成功','count' =>count($list),'data' =>$data);
        return json_encode($arrayName);
    }

    /**
     * 新增部门
     * @return string
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $param = $this->request->param();
            $param['store_id'] = StoreId();
            $verify = input('_verify', true);
            //验证规则
            if($verify!='0'){
                try{
                    $this->validate($param, 'StoreGroup');
                }catch (\Exception $e){
                    $this->error($e->getMessage());
                }
            }
            $result = StoreGroup::create($param);
            if ($result == true) {
                clear_store_cache();//清空整个商户的缓存
                insert_user_log('添加了用户组');
                $this->success('添加成功');
            } else {
                $this->error($this->errorMsg);
            }
        }
        $user = User::with('group')->where('id',UserId())->find();
        return View::fetch('save', [
            'pid' => list_to_level(StoreGroup::get_list(),$user['pid'])
        ]);
    }

    /**
     * 修改部门
     * @param $id 部门ID
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function edit($id)
    {
        if ($this->request->isPost()) {
            $param = $this->request->param();
            $verify = input('_verify', true);
            //验证规则
            if($verify!='0'){
                try{
                    $this->validate($param, 'StoreGroup');
                }catch (\Exception $e){
                    $this->error($e->getMessage());
                }
            }
            //更新数据
            $result = StoreGroup::update($param,['id'=>$param['id']]);
            if ( $result == true) {
                clear_store_cache();//清空整个商户的缓存
                insert_user_log('修改了用户组');
                $this->success('修改成功');
            } else {
                $this->error($this->errorMsg);
            }
        }
        $data  = StoreGroup::where('id',$id)->find();
        $user = User::with('group')->where('id',UserId())->find();
        return $this->fetch('save', [
            'data' => $data,
            'pid' => list_to_level(StoreGroup::get_list(),$user['pid'])
        ]);
    }

    /**
     * 删除部门
     */
    public function del()
    {
        if ($this->request->isPost()) {
            $param = $this->request->param();
            //判断是否有用户
            $data = User::where('group_id',$param['id'])->count();
            if($data>'0'){
                $this->error('请先删除部门下员工');
            }
            //判断是否有子部门
            $group = StoreGroup::where('pid',$param['id'])->count();
            if($group>'0'){
                $this->error('请先删除或调整子部门');
            }
            StoreGroup::destroy($param['id'],true);
            clear_store_cache();//清空整个商户的缓存
            insert_user_log('删除了用户组');
            $this->success('删除成功');
        }
    }

}
