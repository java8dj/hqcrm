<?php
namespace app\store\controller;

use app\common\controller\StoreBase;

use app\common\model\User as UserModel;
use app\store\model\Notice;
use app\store\model\StoreAuth;
use app\store\model\User;

class Index extends StoreBase
{
    protected $noLogin = ['login', 'captcha'];
    protected $noAuth = ['index','home','uploadImage','uploadFile', 'uploadVideo', 'logout','clear','get_notice'];
    
    //后台首页
    public function index()
    {
        //获取导航
        $navbar = list_to_tree(StoreAuth::getNavbar());
        $user = User::with(['store'])->where(['id'=> UserId()])->find();
        return $this->fetch('index',[
            'navbar' => $navbar,
            'user'   => $user,
        ]);
    }
    //首页
    public function home()
    {

        return $this->fetch('home',[

        ]);
    }
    //登陆
    public function login()
    {
        is_user_login() && $this->redirect(url('store/index/index')); // 登录直接跳转
        if ($this->request->isPost()) {
            $param = $this->request->param();
            //是否验证码
            if(get_system('system')['is_captcha']=='1'){
                //验证规则
                try{
                    $this->validate($param, ['captcha|验证码'=>'require|captcha']);
                }catch (\Exception $e){
                    $this->error($e->getMessage());
                }
            }
            //登陆方式
            $user = new UserModel();
            $admin = $user->with('store')->where(['mobile' => $param['username'],'password' => md5($param['password'])])->order('id desc')->find();
            if ($admin) {
                $admin['status'] != 1 && $admin['id'] != $admin['store']['admin_id'] && $this->error('账号已禁用');
                //检查是否过期
                if($admin['exp_time'] < date('Y-m-d',time())){
                    $this->error('您的账户已过期');
                }
                // 保存状态
                $auth = [
                    'user_id' => $admin['id'],
                    'store_id' => $admin['store_id'],
                    'exp_time' => $admin['exp_time'],
                    'mobile' => $admin['mobile'],
                ];
                session('user_auth', $auth);
                session('user_auth_sign', data_auth_sign($auth));
                // 更新信息
                UserModel::update([
                    'last_login_time' => time(),
                    'last_login_ip'   => $this->request->ip(),
                    'login_count'     => $admin['login_count'] + 1,
                ],['id' => $admin['id']]);
                insert_user_log('登录了后台系统');
                $this->success('登录成功', url('@store'));
            } else {
                $this->error('账号或密码错误');
            }
        }
        return $this->fetch('login');
    }

    //验证码
    public function captcha($id='')
    {
        return captcha($id);
    }
    // 清理缓存
    public function clear()
    {
        insert_user_log('清理了缓存');
        //清空整个商户的缓存
        clear_store_cache();
        return $this->success('清理完成');
    }

    /**
     * 系统续费
     */
    public function renew()
    {

        return $this->fetch('renew');
    }

    /**
     * 退出系统
     */
    public function logout()
    {
        insert_user_log('退出了后台系统');
        session('user_auth', null);
        session('user_auth_sign', null);
        return $this->redirect(url('store/index/login'));
    }
    /**
     * 长轮询
     * @return mixed
     */
    public function get_notice()
    {
        $notice = Notice::where('user_id',UserId())->where('status','0')->limit(10)->order('datetime asc')->count();
        $this->result($notice);
    }

}
