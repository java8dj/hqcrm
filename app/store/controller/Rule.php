<?php
namespace app\store\controller;

use app\common\controller\StoreBase;
use app\common\model\Taocan;
use app\store\model\StoreRule;
use app\store\model\StoreAuth;
use app\store\model\User;
use app\store\model\Store;

class Rule extends StoreBase
{
    public function index()
    {
        return $this->fetch('index');
    }
    public function index_json($limit='15')
    {
        $user = new StoreRule();
        $list = $user->order('sort_order desc,id desc')->paginate($limit);
        $this->result($list);
    }

    public function add()
    {
        if ($this->request->isPost()) {
            $param = $this->request->param();
            $param['store_id'] = StoreId();
            $verify = input('_verify', true);
            //验证规则
            if($verify!='0'){
                try{
                    $this->validate($param, 'StoreRule');
                }catch (\Exception $e){
                    $this->error($e->getMessage());
                }
            }
            $result = StoreRule::create($param);
            if ($result == true) {
                clear_store_cache();//清空整个商户的缓存
                insert_user_log('添加了用户组');
                $this->success('添加成功');
            } else {
                $this->error($this->errorMsg);
            }
        }
        //检查所属套餐
        $shop = Store::where('id', StoreId())->find();
        $taocan = Taocan::where('id',$shop['taocan'])->find();
        $userAuthRule = collection(StoreAuth::where('id','in',$taocan['rules'])->where(['status' => 1])->order('sort_order asc')->select())->toArray();
        foreach ($userAuthRule as $k => $v) {
            //$authRule[$k]['open'] = true;
        }
        return $this->fetch('save', ['userAuthRule' => list_to_tree($userAuthRule),'pid' => StoreRule::list_to_level()]);
    }

    public function edit()
    {
        if ($this->request->isPost()) {
            $param = $this->request->param();
            $verify = input('_verify', true);
            //验证规则
            if($verify!='0'){
                try{
                    $this->validate($param, 'StoreRule');
                }catch (\Exception $e){
                    $this->error($e->getMessage());
                }
            }
            //更新数据
            $resule = StoreRule::update($param,['id'=>$param['id']]);
            if ( $resule == true) {
                clear_store_cache();//清空整个商户的缓存
                insert_user_log('修改了用户组');
                $this->success('修改成功');
            } else {
                $this->error($this->errorMsg);
            }
        }
        $data  = StoreRule::where('id', input('id'))->find();
        $shop = Store::where('id',$data['store_id'])->find();
        $taocan = Taocan::where('id',$shop['taocan'])->find();
        $userAuthRule = collection(StoreAuth::where('id','in',$taocan['rules'])->where(['status' => 1])->order('sort_order asc')->select())->toArray();
        foreach ($userAuthRule as $k => $v) {
            // $authRule[$k]['open'] = true;
            $userAuthRule[$k]['checked'] = in_array($v['id'], explode(',', $data['rules']));
        }
        return $this->fetch('save', ['data' => $data, 'userAuthRule' => list_to_tree($userAuthRule),'pid' => StoreRule::list_to_level()]);
    }

    public function del()
    {
        if ($this->request->isPost()) {
            $param = $this->request->param();
            //判断是否有用户
            $data = User::where('group_id',$param['id'])->count();
            if($data>'0'){
                $this->error('请先删除部门下员工');
            }else{
                StoreRule::destroy($param['id']);
                clear_store_cache();//清空整个商户的缓存
                insert_user_log('删除了用户组');
                $this->success('删除成功');
            }
        }
    }
}
