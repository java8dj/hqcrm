<?php
namespace app\store\controller;

use app\common\controller\StoreBase;
use think\facade\Db;
use app\store\model\StoreLog;

class Log extends StoreBase
{
    public function index()
    {
        return $this->fetch('index');
    }
    public function index_json($limit='')
    {
        $list = StoreLog::with(['user'])->order('create_time desc,id desc')->paginate($limit);
        $this->result($list);
    }

    public function truncate()
    {
        if ($this->request->isPost()) {
            Db::name('store_log')->where('store_id', StoreId())->delete(true);
            $this->success('操作成功');
        }
    }

    
}
