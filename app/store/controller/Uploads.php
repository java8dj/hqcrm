<?php
namespace app\store\controller;
date_default_timezone_set("Asia/Chongqing");
error_reporting(E_ERROR);
header("Content-Type: text/html; charset=utf-8");
use app\common\controller\StoreBase;
use think\facade\Filesystem;
use think\facade\Config;
use app\common\model\Uploads as UploadsModel;

class Uploads extends StoreBase
{
    protected $noAuth = [ 'uploadImage', 'uploadFile', 'uploadVideo', 'editor', 'editor_upload'];
    
    public function index()
    {
        $list = UploadsModel::order('id desc')->paginate();
        return $this->fetch('index', ['list' => $list]);
    }

    //上传图片
    public function uploadImage()
    {
        try {
            $file = request()->file('file');
            //处理图片
            UploadsModel::UploadValidate($file);
            $params = get_system('qiniu');
            //判断上传位置
            if($params['type']=='1'){//七牛
                $key = 'store_'.StoreId().'/'.date('Y/md/His_').substr(microtime(), 2, 6).'_'.mt_rand(0,999).'.'.$file->getOriginalExtension();
                $qiniu = new \app\common\library\Qiniu();
                $url = $params['domain'].$qiniu->upload($file->getRealPath(), $key);
                UploadsModel::CreateInfo('qiniu',$params['domain'], $key, $file->getSize(), $file->getOriginalMime());
                insert_user_log('上传了图片');
                return ['code' => 1, 'url' => $url,'msg'=>'上传成功'];
            }else{//默认本地
                $savename = Filesystem::disk('public')->putFile('uploads/store_'.StoreId(),$file);
                $url = request()->domain().'/'.$savename;
                UploadsModel::CreateInfo('local','', $savename, $file->getSize(), $file->getOriginalMime());
                insert_user_log('上传了图片');
                return ['code' => 1, 'url' => $url,'msg'=>'上传成功'];
            }
        } catch (\Exception $e) {
            return ['code' => 0, 'msg' => $e->getMessage()];
        }
    }
    //删除图片
    public function del()
    {
        if ($this->request->isPost()) {
            $param = $this->request->param();
            $files = UploadsModel::where('id',$param['id'])->find();
            //判断存储位置
            if($files->getData('storage')=='qiniu'){
                $qiniuconfig = get_system('qiniu');
                $key = $files['file_name'];
                $qiniu = new \app\common\library\Qiniu();
                $qiniu->delete($key);
                UploadsModel::destroy($param['id']);
                insert_user_log('删除了图片');
                $this->success('删除成功');
            }elseif($files->getData('storage')=='local'){
                $this->success('暂不支持删除本地文件');
            }
            
        }
    }
    //上传文件
    public function uploadFile()
    {
        try {
            $file = request()->file('file');
            $params = get_system('qiniu');
            //判断上传位置
            if($params['type']=='1'){//七牛
                $key = 'store_'.StoreId().'/'.date('Y/md/His_').substr(microtime(), 2, 6).'_'.mt_rand(0,999).'.'.$file->getOriginalExtension();
                $qiniu = new \app\common\library\Qiniu();
                $url = $params['domain'].$qiniu->upload($file->getRealPath(), $key);
                UploadsModel::CreateInfo('qiniu',$params['domain'], $key, $file->getSize(), $file->getOriginalMime());
                insert_user_log('上传了图片');
                return ['code' => 1, 'url' => $url,'msg'=>'上传成功'];
            }else{//默认本地
                $savename = Filesystem::disk('public')->putFile('uploads/store_'.StoreId(),$file);
                $url = request()->domain().'/'.$savename;
                UploadsModel::CreateInfo('local','', $savename, $file->getSize(), $file->getOriginalMime());
                insert_user_log('上传了图片');
                return ['code' => 1, 'url' => $url,'msg'=>'上传成功'];
            }
        } catch (\Exception $e) {
            return ['code' => 0, 'msg' => $e->getMessage()];
        }
    }
    //上传图片
    public function editor_upload()
    {
        try {
            $file = request()->file('file');
            //处理图片
            UploadsModel::UploadValidate($file);
            $params = get_system('qiniu');
            //判断上传位置
            if($params['type']=='1'){//七牛
                $key = 'store_'.StoreId().'/'.date('Y/md/His_').substr(microtime(), 2, 6).'_'.mt_rand(0,999).'.'.$file->getOriginalExtension();
                $qiniu = new \app\common\library\Qiniu();
                $url = $params['domain'].$qiniu->upload($file->getRealPath(), $key);
                UploadsModel::CreateInfo('qiniu',$params['domain'], $key, $file->getSize(), $file->getOriginalMime());
                insert_user_log('上传了图片');
                return json(['location'=>$url]);
            }else{//默认本地
                $savename = Filesystem::disk('public')->putFile('uploads/store_'.StoreId(),$file);
                $url = request()->domain().'/'.$savename;
                UploadsModel::CreateInfo('local','', $savename, $file->getSize(), $file->getOriginalMime());
                insert_user_log('上传了图片');
                return json(['location'=>$url]);
            }
        } catch (\Exception $e) {
            return ['code' => 0, 'msg' => $e->getMessage()];
        }
    }

}
