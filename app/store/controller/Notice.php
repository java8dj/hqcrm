<?php
namespace app\store\controller;

use app\common\controller\StoreBase;
use app\store\model\Notice as Model;
use app\crm\model\CrmCustomer;
use app\store\model\User;

class Notice extends StoreBase
{
    protected $noAuth = ['index','index_json','add','detail','detail_json','edit','del','message','read','unread'];
    /**
     * 通知列表
     * @return mixed
     */
    public function index()
    {
        return $this->fetch('index');
    }
    public function index_json($limit='15')
    {
        $model = new Model();
        $list = $model->with(['user'])->where('user_id',UserId())->order('status asc,id desc')->paginate($limit);
        $this->result($list);
    }

    /**
     * 添加日程
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $param = $this->request->param();
            $param['store_id'] = StoreId();
            $param['user_id'] = UserId();
            $result = Model::create($param);
            if ($result == true) {
                insert_user_log('添加了新的提醒日程');
                $this->success('添加成功');
            } else {
                $this->error($this->errorMsg);
            }
        }
        $notice = Model::typelist();
        return $this->fetch('save', [
            'notice'       => $notice
        ]);
    }

    /**
     * 设置状态
     * @param $id
     * @param $status
     */
    public function edit($id,$status)
    {
        if ($this->request->isPost()) {
            Model::where('id',$id)->update(['status'=>$status,'update_time'=>$status]);
            $this->success('设置成功');
        }
    }

    /**
     * 删除通知
     */
    public function del()
    {
        if ($this->request->isPost()) {
            $param = $this->request->param();
            Model::destroy($param['id']);
            insert_user_log('删除通知');
            $this->success('删除成功');
        }
    }
    /**
     * 设为已读
     */
    public function read()
    {
        if ($this->request->isPost()) {
            $param = $this->request->param();
            $result = Model::where('id','in',$param['id'])->update(['status'=>1,'update_time'=>time()]);
            $this->success('设置成功');
        }
    }
    /**
     * 设为未读
     */
    public function unread()
    {
        if ($this->request->isPost()) {
            $param = $this->request->param();
            $result = Model::where('id','in',$param['id'])->update(['status'=>0,'update_time'=>time()]);
            $this->success('设置成功');
        }
    }
    /**
     * 日程详情
     * @param string $date
     * @return mixed
     */
    public function detail($date='')
    {
        //当前日期
        if(empty($date)){
            $date = date('Y-m-d');
        }
        //日期集
        for($i=0;$i<=10;$i++){
                $list[$i]['day'] = date("Y-m-d",strtotime("+".($i-2)." day",strtotime($date)));
        }
        return $this->fetch('detail',[
            'date'   => $date,
            'list'   => $list
        ]);
    }
    public function detail_json($limit='15',$date='')
    {
        $model = new Model();
        if(empty($date)){
            $date = date('Y-m-d');
        }
        $model = $model->whereDay('datetime',$date);
        $list = $model->with(['user','customer'])->where('user_id',UserId())->order('status asc,id desc')->paginate($limit);
        $this->result($list);
    }

    /**
     * 最新消息
     * @return mixed
     */
    public function message()
    {
        $unread = Model::where('user_id',UserId())->where('status','0')->limit(10)->order('datetime asc')->select();
        $read = Model::where('user_id',UserId())->where('status','1')->limit(10)->order('datetime asc')->select();
        return $this->fetch('message', [
            'unread' => $unread,
            'read'   => $read
        ]);
    }
}