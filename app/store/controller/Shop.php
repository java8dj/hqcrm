<?php
namespace app\store\controller;
use app\common\controller\StoreBase;

use app\store\model\Store as ShopModel;

class Shop extends StoreBase
{
    //机构配置
    public function index()
    {
        if ($this->request->isPost()) {
            $param = $this->request->param();
            //更新数据
            $result = ShopModel::update($param,['id'=> StoreId()]);
            if ( $result == true) {
                insert_user_log('修改了系统信息');
                $this->success('修改成功');
            }else{
                $this->error($this->errorMsg);
            }
        }
        $data = ShopModel::where('id', StoreId())->find();
        return $this->fetch('index', ['data' => $data]);
    }
    
}
