<?php
namespace app\lnadmin\validate;
use think\Validate;

class Store extends Validate
{
    protected $rule = [
        'name' => 'require',
        'taocan' => 'require',
        'mobile' => 'mobile|unique:user',
        'exp_time' => 'require',
    ];

    protected $message = [
        'name.require' => '名称不能为空',
        'taocan.require' => '套餐不能为空',
        'mobile.mobile' => '手机号码格式错误',
        'mobile.unique' => '该手机号码已存在',
        'exp_time.require' => '过期时间不能为空',
    ];
}
