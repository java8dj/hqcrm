<?php
namespace app\lnadmin\controller;

use app\common\controller\AdminBase;
use app\common\model\Dict as DictionaryModel;
use app\common\model\StoreAuth;

class Dict extends AdminBase
{
    protected function _initialize()
    {
        parent::_initialize();
    }

    public function index()
    {
        return $this->fetch('index');
    }
    public function index_json($limit='15')
    {
        $list = DictionaryModel::with('userrule')->order('id desc')->paginate($limit);
        $this->result($list);
    }

    public function add()
    {
        if ($this->request->isPost()) {
            $param = $this->request->param();
            $result = DictionaryModel::create($param);
            if ($result == true) {
                insert_admin_log('添加了字典分类');
                $this->success('添加成功', url('lnadmin/admin/index'));
            } else {
                $this->error($this->errorMsg);
            }
        }
        $userAuthRule = collection(StoreAuth::where(['status' => 1])->order('sort_order asc')->select())->toArray();
        return $this->fetch('save',['auth'=>list_to_level($userAuthRule)]);
    }

    public function edit()
    {
        if ($this->request->isPost()) {
            $param  = $this->request->param();
            //更新数据
            $resule = DictionaryModel::update($param,['id'=>$param['id']]);
            if ( $resule == true) {
                insert_admin_log('修改了字典类型');
                $this->success('修改成功');
            } else {
                $this->error($this->errorMsg);
            }
        }
        $userAuthRule = collection(StoreAuth::where(['status' => 1])->order('sort_order asc')->select())->toArray();
        return $this->fetch('save', [
            'data'  => DictionaryModel::where('id', input('id'))->find(),
            'auth'  => list_to_level($userAuthRule)
        ]);
    }

    public function del()
    {
        if ($this->request->isPost()) {
            $param = $this->request->param();
            DictionaryModel::destroy($param['id']);
            insert_admin_log('删除了字典类型');
            $this->success('删除成功');
            
        }
    }

}
