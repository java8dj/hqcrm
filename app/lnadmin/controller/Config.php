<?php
namespace app\lnadmin\controller;

use app\common\controller\AdminBase;
use app\common\model\System;

class Config extends AdminBase
{
    protected function _initialize()
    {
        parent::_initialize();
    }
    
    public function setting()
    {
        if ($this->request->isPost()) {
            $param = $this->request->param();
            System::where('key','website')->update(['jdata'=>$param]);
            cache('website',null);
            $this->success('设置成功');
        }
        $data = System::where('key','website')->find()['jdata'];
        return $this->fetch('setting', ['data' => $data]);
    }
    
    public function param()
    {
        if ($this->request->isPost()) {
            $param = $this->request->param();
            if(isset($param['type']) and $param['type']=='on'){
                $param['type'] = '1';
            }else{
                $param['type'] = '0';
            }
            System::where('key',$param['model'])->update(['jdata'=>$param]);
            if($param['model'] == 'wxpay'){
                System::writeCertPemFiles($param['certpem'], $param['keypem']);
            }
            cache($param['model'],null);
            $this->success('设置成功');
        }
        $model = input('model')?:'qiniu';
        $list = System::where('key',$model)->find()['jdata'];
        return $this->fetch('param_'.$model, ['list' => $list]);
    }

    public function upload()
    {
        if ($this->request->isPost()) {
            $param = $this->request->param();
            System::where('key','upload_setting')->update(['jdata'=>$param]);
            cache('upload_setting',null);
            insert_admin_log('修改了上传设置');
            $this->success('保存成功');
        }
        $data = System::where('key','upload_setting')->find()['jdata'];
        return $this->fetch('upload',['data' => $data]);
    }
    public function system()
    {
        if ($this->request->isPost()) {
            $param = $this->request->param();
            System::where('key','system')->update(['jdata'=>$param]);
            cache('system',null);
            insert_admin_log('修改了参数设置');
            $this->success('保存成功');
        }
        $data = System::where('key','system')->find()['jdata'];
        return $this->fetch('system',['data' => $data]);
    }
    
    public function web_nav()
    {
        if ($this->request->isPost()) {
            $param = $this->request->param();
            System::where('key','web_nav')->update(['jdata'=>$param]);
            cache('web_nav',null);
            $this->success('设置成功');
        }
        $data = System::where('key','web_nav')->find()['jdata'];
        return $this->fetch('web_nav', ['data' => $data]);
    }
}
