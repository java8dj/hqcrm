<?php
namespace app\lnadmin\controller;
use app\common\controller\AdminBase;
use app\common\model\StoreAuth as Model;

class StoreAuth extends AdminBase
{
    /**
     * 权限列表
     * @return mixed
     */
    public function rule()
    {
        return $this->fetch('rule');
    }
    public function rule_json()
    {
        $authRule = collection(Model::order('sort_order asc')->select())->toArray();
        $count = collection(Model::order('sort_order asc')->select())->count();
        foreach ($authRule as $k => $v) {
            $data[$k] = array(
                  'authorityId' =>$v['id'],
                  "authorityName" => $v['name'],
                  "authority" =>$v['url'],
                  "menuUrl" =>$v['url'],
                  "parentId" =>$v['pid'],
                  "isMenu" =>$v['type']=='nav'?0:1,
                  "orderNumber" =>$v['sort_order'],
                  "menuIcon" =>$v['icon'],
                  "status" =>$v['status'],
                  "createTime" =>'',
                  "updateTime" =>'',
                  "open"=> $v['status']?true:false
                );
        }
        $arrayName = array('code' =>0,'msg' =>'加载成功','count' =>$count,'data' =>$data);
        return json_encode($arrayName);
    }

    /**
     * 新增权限
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $result = Model::create($this->request->param());
            if ($result == true) {
                insert_admin_log('添加了用户权限规则');
                $this->success('添加成功');
            } else {
                $this->error($this->errorMsg);
            }
        }
        $authRule = Model::where(['status' => 1])->order('sort_order asc')->select();
        return $this->fetch('saveRule', ['authRule' => list_to_level($authRule)]);
    }

    /**
     * 编辑权限
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function edit()
    {
        if ($this->request->isPost()) {
            $result = Model::update($this->request->param());
            if ($result == true) {
                insert_admin_log('修改了用户权限规则');
                $this->success('修改成功');
            } else {
                $this->error($this->errorMsg);
            }
        }
        $authRule = Model::where(['status' => 1])->order('sort_order asc')->select();
        return $this->fetch('saveRule', ['data' => Model::where('id', input('id'))->find(),'authRule' => list_to_level($authRule)]);
    }

    /**
     * 删除权限
     */
    public function del()
    {
        if ($this->request->isPost()) {
            $param = $this->request->param();
            Model::where('pid', input('id'))->count() && $this->error('请先删除子节点');
            Model::destroy($param['id']);
            insert_admin_log('删除了用户权限规则');
            $this->success('删除成功');
        }
    }
}
