<?php
namespace app\lnadmin\model;
use think\Model;
use EasyWeChat\Factory;
use think\facade\Request;
use think\facade\Db;
use think\facade\Cache;
use think\facade\Session;
use think\facade\Config;
use think\model\concern\SoftDelete;

class Wechat extends Model
{
    use SoftDelete;
    protected $deleteTime = 'delete_time';
    protected $defaultSoftDelete = 0;
    protected $autoWriteTimestamp = true;
    
   
    //小程序API
    public static function wxapp()
    {
        $wxapp = get_system('wxapp');
        $appid = $wxapp['appid'];
        $secret = $wxapp['appsecret'];
        
        $config = [
            'app_id' => $appid,
            'secret' => $secret,
            
            'response_type' => 'array',
            'log' => [
                'level' => 'debug',
                'file' => \think\facade\Env::get('runtime_path').'wechat.log',
            ],
        ];
        $app = Factory::miniProgram($config);
        return $app;
    }
    //公众号API
    public static function weixin()
    {
        $weixin = get_system('weixin');
        $config = [
            'app_id'  => $weixin['appid'],
            'secret'  => $weixin['appsecret'],
            'token'   => $weixin['token'],          // Token
            'aes_key' => $weixin['aes_key'], 
            'log' => [
                'level' => 'debug',
                'file' => \think\facade\Env::get('runtime_path').'wechat.log',
            ],
        ];
        $app = Factory::officialAccount($config);
        return $app;
    }
    //微信支付参数
    public static function wxpay()
    {
        $weixin = get_system('wxpay');
        $appid = $weixin['appid'];
        $mch_id = $weixin['mch_id'];
        $key = $weixin['key'];

        $config = [
            // 必要配置
            'app_id'             => $appid,
            'mch_id'             => $mch_id,
            'key'                => $key,   // API 密钥
            // 如需使用敏感接口（如退款、发送红包等）需要配置 API 证书路径(登录商户平台下载 API 证书)
            'cert_path'          => base_path().'/common/library/wechat/cert/cert.pem', // XXX: 绝对路径！！！！
            'key_path'           => base_path().'/common/library/wechat/cert/key.pem',      // XXX: 绝对路径！！！！
            'notify_url'         => $weixin['notify_url'],     // 你也可以在下单时单独设置来想覆盖它
        ];

        $app = Factory::payment($config);
        $jssdk = $app->jssdk;
        return $jssdk;
    }
    //微信支付参数
    public static function payminiapp()
    {
        $weixin = get_system('wxpay');
        $appid = $weixin['appid'];
        $mch_id = $weixin['mch_id'];
        $key = $weixin['key'];

        $config = [
            // 必要配置
            'app_id'             => $appid,
            'mch_id'             => $mch_id,
            'key'                => $key,   // API 密钥
            // 如需使用敏感接口（如退款、发送红包等）需要配置 API 证书路径(登录商户平台下载 API 证书)
            'cert_path'          => base_path().'/common/library/wechat/cert/cert.pem', // XXX: 绝对路径！！！！
            'key_path'           => base_path().'/common/library/wechat/cert/key.pem',      // XXX: 绝对路径！！！！
            'notify_url'         => $weixin['notify_url'],     // 你也可以在下单时单独设置来想覆盖它
        ];

        $app = Factory::payment($config);
        return $app;
    }
    //获取ACCESSTOKEN
    public static function access_token($true = false)
    {
        $app = self::weixin();
        $accessToken = $app->access_token;
        $token = $accessToken->getToken($true);
        return $token['access_token'];
    }
    
    
}