<?php
namespace app\common\model;
use think\Model;
use think\model\concern\SoftDelete;

class User extends Model
{
    use SoftDelete;
    protected $deleteTime = 'delete_time';
    protected $defaultSoftDelete = 0;

    public function setPasswordAttr($value)
    {
        return md5($value);
    }

    /**
     * 关联部门
     * @return \think\model\relation\BelongsTo
     */
    public function group()
    {
        return $this->belongsTo(StoreGroup::class, 'group_id', 'id')->bind(['rules','group_name'=>'name']);
    }

    /**
     * 关联商户
     * @return \think\model\relation\BelongsTo
     */
    public function store()
    {
        return $this->belongsTo(Store::class, 'store_id', 'id')->bind(['store_name'=>'name','exp_time','store_logo'=>'logo']);
    }



}