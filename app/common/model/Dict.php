<?php
namespace app\common\model;
use think\Model;
use think\facade\Db;

class Dict extends Model
{
    
    public function userrule()
    {
        return $this->belongsTo(StoreAuth::class, 'auth_id', 'id')->bind(['auth_name'=>'name']);
    }
    
}