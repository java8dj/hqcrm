<?php
namespace app\common\model;
use think\Model;
use think\facade\Db;
use think\model\concern\SoftDelete;

class Store extends Model
{
    use SoftDelete;
    protected $deleteTime = 'delete_time';
    protected $defaultSoftDelete = 0;

    protected $json = ['wechat','wxapp','wxpay'];
    protected $jsonAssoc = true;

    protected $type = [
        'exp_time'     => 'timestamp:Y-m-d',
    ];

    /**
     * 关联日志表
     * @return \think\model\relation\HasMany
     */
    public function log()
    {
        return $this->hasMany(StoreLog::class, 'store_id', 'id');
    }

    /**
     * 关联套餐表
     * @return \think\model\relation\BelongsTo
     */
    public function taocan()
    {
        return $this->belongsTo(Taocan::class, 'taocan', 'id')->bind(['taocan_name'=>'name']);
    }


}