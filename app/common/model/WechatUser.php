<?php
namespace app\common\model;
use think\Model;
use EasyWeChat\Factory;
use think\model\concern\SoftDelete;

class WechatUser extends Model
{
    use SoftDelete;
    protected $deleteTime = 'delete_time';
    protected $defaultSoftDelete = 0;
    
    public static function get_lists()
    {
        $list = self::where('subscribe',1)->select();
        return $list;
    }
    
    public function getGenderAttr($value)
    {
        $name = [''=>'未知','0'=>'未知','1'=>'男','2'=>'女'];
        return $name[$value];
    }
    public function getSubscribeSceneAttr($value)
    {
        $name = [
            ''=>'未知',
            'ADD_SCENE_SEARCH'=>'公众号搜索',
            'ADD_SCENE_ACCOUNT_MIGRATION'=>'公众号迁移',
            'ADD_SCENE_PROFILE_CARD'=>'名片分享',
            'ADD_SCENE_QR_CODE'=>'扫描二维码',
            'ADD_SCENE_PROFILE_LINK'=>'图文页内名称点击',
            'ADD_SCENE_PROFILE_ITEM'=>'图文页右上角菜单',
            'ADD_SCENE_PAID'=>'支付后关注',
            'ADD_SCENE_OTHERS'=>'其他',
        ];
        return $name[$value];
    }
    
    public function getSubscribeTimeAttr($value) {
        return date('Y-m-d H:i:s',$value);
    }

    /**
     * 更新所有OPENID
     */
    static function UpdateOpenid($openid)
    {
        //将所有openid读出来,并写入缓存
        $old_openid = self::column('openid');
        //计算差集
        $list = array_diff($openid,$old_openid);
        $result = [];
        if(!empty($list)){
            foreach ($list as $k=>$r){
                $data[$k]['store_id'] = StoreId();
                $data[$k]['openid'] = $r;
            }
            $model = new self();
            $result = $model->saveAll($data);
        }
        return count($result);
    }
    //更新数据
    public static function UpdateUser($openid)
    {
        //检查是否存在该记录
        $data = self::where('openid',$openid)->find();
        //拉取该用户信息
        $wxapp = wxapi(StoreId(),'weixin');
        $user = $wxapp->user->get($openid);
        if($data){
            //判断是否取消关注
            if($user['subscribe'] == '0'){
                self::where('openid',$openid)->update(['subscribe'=>'0']);
            }
        }else{
            //判断是否取消关注
            if($user['subscribe'] != '0'){
                //创建用户信息
                self::create([
                    'store_id'       => StoreId(),
                    'openid'         => $openid,
                    'nickname'       => !empty($user['nickname'])?$user['nickname']:'',
                    'gender'         => !empty($user['sex'])?$user['sex']:'',
                    'country'        => !empty($user['country'])?$user['country']:'',
                    'province'       => !empty($user['province'])?$user['province']:'',
                    'city'           => !empty($user['city'])?$user['city']:'',
                    'avatarurl'      => !empty($user['headimgurl'])?$user['headimgurl']:'',
                    'unionid'        => !empty($user['unionid'])?$user['unionid']:'',
                    'subscribe'      => !empty($user['subscribe'])?$user['subscribe']:'',
                    'subscribe_time' => !empty($user['subscribe_time'])?$user['subscribe_time']:'',
                    'subscribe_scene'=> !empty($user['subscribe_scene'])?$user['subscribe_scene']:'',
                ]);
            }
            
        }
        return;
    }
    
}