<?php
namespace app\common\model;

use think\Model;
use think\Request;

class StoreLog extends Model
{
    /**
     * 关联商户
     * @return \think\model\relation\BelongsTo
     */
    public function store()
    {
        return $this->belongsTo(Store::class,'store_id','id');
    }

    /**
     * 关联用户
     * @return \think\model\relation\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }
    
    
    
}