<?php
namespace app\common\model;
use think\Model;
use think\model\concern\SoftDelete;

class CrmCustomer extends Model
{
    use SoftDelete;
    protected $deleteTime = 'delete_time';
    protected $defaultSoftDelete = 0;

    protected $globalScope = ['store_id'];
    public function scopeStore_id($query)
    {
        $query->where('store_id', StoreId())->where('user_id',0);
    }

    protected $type = [
        'birthday'     => 'timestamp:Y-m-d',
        'draw_time'     => 'timestamp:Y-m-d',
    ];

    /**
     * 读取手机号
     * @param $value
     * @return mixed|string
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function getMobileAttr($value)
    {
        $data = crm_setting('crm');
        if(isset($data['pool_mobile']) && $data['pool_mobile'] == 1){
            $value = str_replaces($value,3,4,'*');
        }
        return $value;
    }

    /**
     * 关联部门表
     * @return \think\model\relation\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * 关联商户表
     * @return \think\model\relation\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(DictData::class, 'category_id', 'id')->bind(['category'=>'name']);
    }
    /**
     * 关联商户表
     * @return \think\model\relation\BelongsTo
     */
    public function from()
    {
        return $this->belongsTo(DictData::class, 'from_id', 'id')->bind(['from'=>'name']);
    }
    /**
     * 关联商户表
     * @return \think\model\relation\BelongsTo
     */
    public function industry()
    {
        return $this->belongsTo(DictData::class, 'industry_id', 'id')->bind(['industry'=>'name']);
    }

    public function setProvinceAttr($value,$data)
    {
        if(!$data['shengshi']){
            $list = array();
        }else{
            $list = explode(',',$data['shengshi']);
        }
        return $list[0];
    }

    public function setCityAttr($value,$data)
    {
        if(!$data['shengshi']){
            $list = array();
        }else{
            $list = explode(',',$data['shengshi']);
        }
        return $list[1];
    }

    public function setRegionAttr($value,$data)
    {
        if(!$data['shengshi']){
            $list = array();
        }else{
            $list = explode(',',$data['shengshi']);
        }
        return $list[2];
    }
    /**
     * 获取列表
     * @return \think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    static function get_list() {
        return self::order('sort_order desc,id desc')->select();
    }

    /**
     * 客户状态
     * @return array[]
     */
    static function get_status()
    {
        $data = [
            ['id' => 1, 'name' => '成交客户'],
            ['id' => 2, 'name' => '目标客户'],
            ['id' => 3, 'name' => '潜在客户'],
            ['id' => 4, 'name' => '流失客户'],
            ['id' => 5, 'name' => '非客户'],
        ];
        return $data;
    }

    /**
     *
     * @param $value
     * @param $data
     * @return string
     */
    public function getStatusAttr($value,$data)
    {
        $status = self::get_status();
        return $status[$data['status']]['name'];
    }
}