<?php
namespace app\common\model;
use think\Model;
use think\model\concern\SoftDelete;

class Taocan extends Model
{
    use SoftDelete;
    protected $deleteTime = 'delete_time';
    protected $defaultSoftDelete = 0;

    /**
     * 获取列表
     * @return \think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function get_list()
    {
        $list = self::where('status','1')->select();
        return $list;
    }
    
}