alter table hq_dict add is_level tinyint(5) NOT NULL DEFAULT 0 COMMENT '是否多级 0否 1是';
alter table hq_dict add is_image tinyint(5) NOT NULL DEFAULT 0 COMMENT '是否图片 0否 1是';
alter table hq_dict add sort_order tinyint(5) NOT NULL DEFAULT 10 COMMENT '排序';
alter table hq_dict_data add pid int(5) NOT NULL DEFAULT 0 COMMENT '上级ID';
alter table hq_dict_data add image varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片';
alter table hq_dict_data add remark varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述';