# HQCRM最简易的客户管理系统(配套小程序)

#### 介绍
    HQCRM是一款多商户的客户管理系统 仅提供最基本的客户管理.客户维护等功能,直接安装直接使用.不需要修改文件....
仅在功能不能满足您的需求时,再进行二开吧!
    配套小程序!


#### 软件架构

1、Thinkphp6.0.5
2、layui
3、easywechat（已内置，暂未开发）

#### 安装教程

新版如果出现500错误,请配置根目录下的 .env 文件,配置数据库连接信息.或者将env 改成其他名字
1.  上传所有文件到服务器,并设置 public 为根目录
2.  将mysql.sql导入数据库,并修改/config/database.php里的数据库连接参数
3.  网站前台 http://你的域名  账号密码需要后台开
4.  网站后台 http://你的域名/lnadmin   默认账号 hqs316 默认密码 123456

#### 使用说明

1.  登陆后台(例如http://crm.ennn.cn/lnadmin)  
2.  点击左边"商户管理",新增商户
3.  打开前台(例如http://crm.ennn.cn/)  
4.  输入您刚刚设置的账号密码

#### 常见问题

1.  服务器配置
    答: PHP版本7.3 以上，Mysql版本 5.6以上
2.  已安装好,却打不开?
    答：请确定伪静态设置了？
3.  出现500错误咋办?
    答：请配置根目录下的 .env 文件,配置数据库连接信息.或者将env 改成其他名字
#### 测试体验

    体验地址：http://crm.ennn.cn
    体验账号: 13459401111
    体验密码: 123456 


    QQ群 : 10110451
    微信 : huangqing

