/*
 Navicat Premium Data Transfer

 Source Server         : 腾讯基础版
 Source Server Type    : MySQL
 Source Server Version : 50718
 Source Host           : mysql-1.ennn.cn:10130
 Source Schema         : crm_ennn_cn

 Target Server Type    : MySQL
 Target Server Version : 50718
 File Encoding         : 65001

 Date: 09/01/2021 12:35:19
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for hq_admin
-- ----------------------------
DROP TABLE IF EXISTS `hq_admin`;
CREATE TABLE `hq_admin`  (
  `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '管理员用户名',
  `password` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '管理员密码',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '0禁用/1启动',
  `last_login_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '上次登录时间',
  `last_login_ip` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '上次登录IP',
  `login_count` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '登录次数',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `create_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '管理员' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of hq_admin
-- ----------------------------
INSERT INTO `hq_admin` VALUES (1, 'admin', 'e10adc3949ba59abbe56e057f20f883e', 1, 1610098598, '123.139.27.125', 1153, 'd', 1555249039, 1610098598);

-- ----------------------------
-- Table structure for hq_admin_log
-- ----------------------------
DROP TABLE IF EXISTS `hq_admin_log`;
CREATE TABLE `hq_admin_log`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `admin_id` smallint(5) UNSIGNED NOT NULL DEFAULT 0 COMMENT '管理员id',
  `username` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '管理员用户名',
  `useragent` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'User-Agent',
  `ip` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'ip地址',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '请求链接',
  `method` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '请求类型',
  `type` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '资源类型',
  `param` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '请求参数',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '日志备注',
  `create_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 490 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '管理员日志' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of hq_admin_log
-- ----------------------------

-- ----------------------------
-- Table structure for hq_auth_group
-- ----------------------------
DROP TABLE IF EXISTS `hq_auth_group`;
CREATE TABLE `hq_auth_group`  (
  `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `rules` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '权限组' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of hq_auth_group
-- ----------------------------
INSERT INTO `hq_auth_group` VALUES (1, '超级管理员', '', 1, '6,43,44,110,111,120,121,122,112,117,118,119,113,114,115,116,125,126,127,128,129,130,131,132,4,123,92,124,45,68,69,70,103,5,16,37,38,39,18,53,17,40,41,42,15,22,23,24');

-- ----------------------------
-- Table structure for hq_auth_group_access
-- ----------------------------
DROP TABLE IF EXISTS `hq_auth_group_access`;
CREATE TABLE `hq_auth_group_access`  (
  `uid` smallint(5) UNSIGNED NOT NULL DEFAULT 0,
  `group_id` smallint(5) UNSIGNED NOT NULL DEFAULT 0
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '权限授权' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of hq_auth_group_access
-- ----------------------------
INSERT INTO `hq_auth_group_access` VALUES (1, 1);
INSERT INTO `hq_auth_group_access` VALUES (2, 1);

-- ----------------------------
-- Table structure for hq_auth_rule
-- ----------------------------
DROP TABLE IF EXISTS `hq_auth_rule`;
CREATE TABLE `hq_auth_rule`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pid` int(11) UNSIGNED NOT NULL DEFAULT 0,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `icon` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `sort_order` int(11) NOT NULL DEFAULT 0 COMMENT '排序',
  `type` char(4) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'nav,auth',
  `index` tinyint(1) NOT NULL DEFAULT 0 COMMENT '快捷导航',
  `status` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 138 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '权限规则' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of hq_auth_rule
-- ----------------------------
INSERT INTO `hq_auth_rule` VALUES (4, 0, '系统设置', '', 'layui-icon-set', 5, 'nav', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (5, 0, '管理员管理', '', 'layui-icon-auz', 6, 'nav', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (6, 0, '控制台', 'lnadmin/index/home', 'layui-icon-home', 1, 'nav', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (15, 5, '权限规则', 'lnadmin/auth/rule', 'fa fa-th-list', 3, 'nav', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (16, 5, '管理员列表', 'lnadmin/admin/index', 'fa fa-user', 0, 'nav', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (17, 5, '角色管理', 'lnadmin/auth/group', 'fa fa-users', 2, 'nav', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (18, 5, '操作日志', 'lnadmin/admin/log', 'fa fa-clock-o', 1, 'nav', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (22, 15, '添加', 'lnadmin/auth/addRule', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (23, 15, '编辑', 'lnadmin/auth/editRule', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (24, 15, '删除', 'lnadmin/auth/delRule', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (37, 16, '添加', 'lnadmin/admin/add', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (38, 16, '编辑', 'lnadmin/admin/edit', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (39, 16, '删除', 'lnadmin/admin/del', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (40, 17, '添加', 'lnadmin/auth/addGroup', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (41, 17, '编辑', 'lnadmin/auth/editGroup', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (42, 17, '删除', 'lnadmin/auth/delGroup', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (43, 6, '修改密码', 'lnadmin/index/editPassword', '', 2, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (44, 6, '清除缓存', 'lnadmin/index/clear', '', 3, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (45, 4, '上传设置', 'lnadmin/config/upload', 'fa fa-upload', 4, 'nav', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (53, 18, '一键清空', 'lnadmin/admin/truncate', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (68, 45, '上传图片', 'lnadmin/index/uploadimage', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (69, 45, '上传文件', 'lnadmin/index/uploadfile', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (70, 45, '上传视频', 'lnadmin/index/uploadvideo', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (92, 4, '配置信息', 'lnadmin/config/param', '', 2, 'nav', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (103, 4, '上传管理', 'lnadmin/uploads/index', '', 10, 'nav', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (110, 0, '商户管理', 'lnadmin/store/index', 'layui-icon-user', 2, 'nav', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (111, 110, '商户列表', 'lnadmin/store/index', '', 0, 'nav', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (112, 110, '套餐管理', 'lnadmin/store/taocan', '', 0, 'nav', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (113, 110, '权限列表', 'lnadmin/store_auth/rule', '', 0, 'nav', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (114, 113, '添加', 'lnadmin/store_auth/add', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (115, 113, '编辑', 'lnadmin/store_auth/edit', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (116, 113, '删除', 'lnadmin/store_auth/del', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (117, 112, '添加', 'lnadmin/store/add_taocan', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (118, 112, '编辑', 'lnadmin/store/edit_taocan', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (119, 112, '删除', 'lnadmin/store/del_taocan', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (120, 111, '新增', 'lnadmin/store/add', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (121, 111, '编辑', 'lnadmin/store/edit', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (122, 111, '删除', 'lnadmin/store/del', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (123, 4, '系统信息', 'lnadmin/config/setting', '', 0, 'nav', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (124, 4, '系统设置', 'lnadmin/config/system', '', 3, 'nav', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (125, 110, '字典管理', 'lnadmin/dict/index', '', 0, 'nav', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (126, 125, '新增', 'lnadmin/dict/add', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (127, 125, '编辑', 'lnadmin/dict/edit', '', 0, 'auth', 0, 1);
INSERT INTO `hq_auth_rule` VALUES (128, 125, '删除', 'lnadmin/dict/del', '', 0, 'auth', 0, 1);

-- ----------------------------
-- Table structure for hq_crm_contract
-- ----------------------------
DROP TABLE IF EXISTS `hq_crm_contract`;
CREATE TABLE `hq_crm_contract`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `store_id` int(10) NOT NULL DEFAULT 0 COMMENT '商户ID',
  `customer_id` int(10) NOT NULL DEFAULT 0 COMMENT '客户ID',
  `contract_no` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '合同编号',
  `user_id` int(10) NOT NULL DEFAULT 0 COMMENT '签单人',
  `datetime` int(11) NOT NULL DEFAULT 0 COMMENT '签单日期',
  `start_time` int(11) NOT NULL DEFAULT 0 COMMENT '起始日期',
  `end_time` int(11) NOT NULL DEFAULT 0 COMMENT '到期时间',
  `file` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '合同文件',
  `photo` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '合同照片',
  `product` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '关联产品',
  `price` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '合同金额',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '合同备注',
  `get_product` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '不可删,读取关联产品',
  `create_time` int(11) NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(11) NOT NULL DEFAULT 0 COMMENT '更新时间',
  `delete_time` int(11) NOT NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '客户 合同表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of hq_crm_contract
-- ----------------------------

-- ----------------------------
-- Table structure for hq_crm_customer
-- ----------------------------
DROP TABLE IF EXISTS `hq_crm_customer`;
CREATE TABLE `hq_crm_customer`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `store_id` int(10) NOT NULL DEFAULT 0 COMMENT '商户ID',
  `user_id` int(10) NOT NULL DEFAULT 0 COMMENT '用户ID',
  `company` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公司名称',
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `mobile` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '电话',
  `wechat` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '微信',
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `phone` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '固话',
  `province` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '省',
  `city` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '市',
  `region` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '区',
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '地址',
  `lat` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '坐标LAT',
  `lng` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '坐标LNG',
  `birthday` int(11) NOT NULL DEFAULT 0 COMMENT '出生年月',
  `category_id` int(10) NOT NULL DEFAULT 0 COMMENT '客户分类',
  `from_id` int(10) NOT NULL DEFAULT 0 COMMENT '客户来源',
  `industry_id` int(10) NOT NULL DEFAULT 0 COMMENT '客户行业',
  `tag` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '客户标签',
  `status` int(10) NOT NULL DEFAULT 0 COMMENT '客户状态',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '客户备注',
  `sort_order` int(10) NULL DEFAULT 100 COMMENT '排序',
  `draw_time` int(11) NOT NULL DEFAULT 0 COMMENT '公海领取时间',
  `create_time` int(11) NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(11) NOT NULL DEFAULT 0 COMMENT '更新时间',
  `delete_time` int(11) NOT NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 107 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '客户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of hq_crm_customer
-- ----------------------------

-- ----------------------------
-- Table structure for hq_crm_field
-- ----------------------------
DROP TABLE IF EXISTS `hq_crm_field`;
CREATE TABLE `hq_crm_field`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `store_id` int(10) NOT NULL DEFAULT 0 COMMENT '商户ID',
  `type` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类型',
  `field` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字段参数',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字段名称',
  `is_status` tinyint(5) NOT NULL DEFAULT 1 COMMENT '状态 0不启用 1启用',
  `is_show` tinyint(5) NOT NULL DEFAULT 1 COMMENT '列表显示 0不显示 1显示',
  `is_required` tinyint(5) NOT NULL DEFAULT 0 COMMENT '必填 0否 1是',
  `sort_order` int(10) NOT NULL DEFAULT 100 COMMENT '排序',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 118 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '客户 字段列表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of hq_crm_field
-- ----------------------------

-- ----------------------------
-- Table structure for hq_crm_setting
-- ----------------------------
DROP TABLE IF EXISTS `hq_crm_setting`;
CREATE TABLE `hq_crm_setting`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `store_id` int(10) NOT NULL DEFAULT 0 COMMENT '商户ID',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '设置名称',
  `jdata` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'JDATA',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '客户 设置表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of hq_crm_setting
-- ----------------------------
INSERT INTO `hq_crm_setting` VALUES (2, 177, 'crm', '{\"pool_import\":\"1\",\"pool_mobile\":\"1\",\"pool_drawnum\":\"0\",\"pool_recovery\":\"0\",\"crm_mobile\":\"1\"}');
INSERT INTO `hq_crm_setting` VALUES (3, 178, 'crm', '{\"pool_import\":\"1\",\"pool_mobile\":\"1\",\"pool_drawnum\":\"0\",\"pool_recovery\":\"0\",\"crm_mobile\":\"1\"}');

-- ----------------------------
-- Table structure for hq_crm_track
-- ----------------------------
DROP TABLE IF EXISTS `hq_crm_track`;
CREATE TABLE `hq_crm_track`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `store_id` int(10) NOT NULL DEFAULT 0 COMMENT '商户ID',
  `customer_id` int(10) NOT NULL DEFAULT 0 COMMENT '客户ID',
  `user_id` int(10) NULL DEFAULT NULL COMMENT '操作用户',
  `type` tinyint(5) NOT NULL DEFAULT 1 COMMENT '类型 0系统记录 1用户记录',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注信息',
  `create_time` int(11) NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(11) NOT NULL DEFAULT 0 COMMENT '更新时间',
  `delete_time` int(11) NOT NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 80 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '客户 跟进表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of hq_crm_track
-- ----------------------------

-- ----------------------------
-- Table structure for hq_dict
-- ----------------------------
DROP TABLE IF EXISTS `hq_dict`;
CREATE TABLE `hq_dict`  (
  `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `auth_id` int(3) NOT NULL DEFAULT 0 COMMENT '所属权限',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字典类目名称',
  `is_level` tinyint(5) NOT NULL DEFAULT 0 COMMENT '是否多级 0否 1是',
  `is_image` tinyint(5) NOT NULL DEFAULT 0 COMMENT '是否图片 0否 1是',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '字典' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of hq_dict
-- ----------------------------
INSERT INTO `hq_dict` VALUES (13, 183, '客户分类', 0, 0);
INSERT INTO `hq_dict` VALUES (14, 183, '客户来源', 0, 0);
INSERT INTO `hq_dict` VALUES (15, 183, '客户行业', 0, 0);
INSERT INTO `hq_dict` VALUES (16, 194, '产品管理', 0, 0);

-- ----------------------------
-- Table structure for hq_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `hq_dict_data`;
CREATE TABLE `hq_dict_data`  (
  `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `store_id` int(3) NOT NULL DEFAULT 0 COMMENT '商家ID',
  `dict_id` int(3) NOT NULL DEFAULT 0 COMMENT '字典ID',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字典名称',
  `sort_order` tinyint(10) NOT NULL DEFAULT 100 COMMENT '排序',
  `value` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '其他值',
  `is_default` tinyint(10) NOT NULL DEFAULT 0 COMMENT '是否默认 0否 1是',
  `status` tinyint(10) NOT NULL DEFAULT 1 COMMENT '状态 0禁用 1启用',
  `create_time` int(10) NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(10) NOT NULL DEFAULT 0 COMMENT '更新时间',
  `delete_time` int(10) NOT NULL DEFAULT 0 COMMENT '删除时间',
  `pid` int(5) NOT NULL DEFAULT 0 COMMENT '上级ID',
  `image` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2191 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '字典数据' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of hq_dict_data
-- ----------------------------

-- ----------------------------
-- Table structure for hq_notice
-- ----------------------------
DROP TABLE IF EXISTS `hq_notice`;
CREATE TABLE `hq_notice`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `store_id` int(10) NOT NULL DEFAULT 0 COMMENT '商户ID',
  `customer_id` int(10) NOT NULL DEFAULT 0 COMMENT '客户ID',
  `user_id` int(10) NOT NULL DEFAULT 0 COMMENT '用户ID',
  `datetime` int(11) NOT NULL DEFAULT 0 COMMENT '日期',
  `type` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类型',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '消息内容',
  `status` tinyint(5) NOT NULL DEFAULT 0 COMMENT '状态 0待办 1已办',
  `create_time` int(11) NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(11) NOT NULL DEFAULT 0 COMMENT '更新时间',
  `delete_time` int(11) NOT NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '消息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of hq_notice
-- ----------------------------

-- ----------------------------
-- Table structure for hq_store
-- ----------------------------
DROP TABLE IF EXISTS `hq_store`;
CREATE TABLE `hq_store`  (
  `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '商户名称',
  `logo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'LOGO',
  `contact` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `mobile` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `province` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '省',
  `city` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '市',
  `area` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '区',
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '地址',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `taocan` int(3) NOT NULL DEFAULT 0 COMMENT '套餐ID',
  `admin_id` int(3) NOT NULL DEFAULT 0 COMMENT '管理员ID',
  `exp_time` int(11) NOT NULL DEFAULT 0 COMMENT '过期时间',
  `status` int(3) NOT NULL DEFAULT 1 COMMENT '状态',
  `wechat` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '公众号',
  `wxapp` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '小程序',
  `wxpay` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '微信支付',
  `create_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  `delete_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 179 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '商家列表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of hq_store
-- ----------------------------
INSERT INTO `hq_store` VALUES (177, '六诺科技', 'https://crm.ennn.cn/uploads/store_177/20210105/e1a8808923bd742281f20ef92b5e68b0.jpg', '黄青', '13459401111', '福建省', '莆田市', '荔城区', '', '211', 5, 293, 2147483647, 1, '{\"appid\":\"wx97b86655ce66c\",\"appsecret\":\"5d66e5b7c1596fdae853f131af96ea34\"}', '{\"appid\":\"\",\"appsecret\":\"\"}', '{\"mch_id\":\"\",\"key\":\"\",\"certpem\":\"\",\"keypem\":\"\"}', 1608815252, 1609856339, 0);

-- ----------------------------
-- Table structure for hq_store_auth
-- ----------------------------
DROP TABLE IF EXISTS `hq_store_auth`;
CREATE TABLE `hq_store_auth`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pid` int(11) UNSIGNED NOT NULL DEFAULT 0,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `icon` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `sort_order` int(11) NOT NULL DEFAULT 0 COMMENT '排序',
  `type` char(4) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'nav,auth',
  `index` tinyint(1) NOT NULL DEFAULT 0 COMMENT '快捷导航',
  `status` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 223 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商户 权限规则' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of hq_store_auth
-- ----------------------------
INSERT INTO `hq_store_auth` VALUES (1, 0, '工作台', '', 'layui-icon-home', 0, 'nav', 0, 1);
INSERT INTO `hq_store_auth` VALUES (2, 0, '成员管理', '', 'layui-icon-user', 9, 'nav', 0, 1);
INSERT INTO `hq_store_auth` VALUES (3, 2, '成员列表', 'store/user/index', '', 0, 'nav', 0, 1);
INSERT INTO `hq_store_auth` VALUES (4, 2, '部门设置', 'store/group/index', '', 0, 'nav', 0, 1);
INSERT INTO `hq_store_auth` VALUES (5, 2, '操作日志', 'store/log/index', '', 5, 'nav', 0, 1);
INSERT INTO `hq_store_auth` VALUES (6, 3, '添加', 'store/user/add', '', 0, 'auth', 0, 1);
INSERT INTO `hq_store_auth` VALUES (7, 3, '编辑', 'store/user/edit', '', 0, 'auth', 0, 1);
INSERT INTO `hq_store_auth` VALUES (8, 3, '删除', 'store/user/del', '', 0, 'auth', 0, 1);
INSERT INTO `hq_store_auth` VALUES (9, 4, '添加', 'store/group/add', '', 0, 'auth', 0, 1);
INSERT INTO `hq_store_auth` VALUES (10, 4, '编辑', 'store/group/edit', '', 0, 'auth', 0, 1);
INSERT INTO `hq_store_auth` VALUES (11, 4, '删除', 'store/group/del', '', 0, 'auth', 0, 1);
INSERT INTO `hq_store_auth` VALUES (12, 5, '一键清空', 'store/log/truncate', '', 0, 'auth', 0, 1);
INSERT INTO `hq_store_auth` VALUES (13, 0, '系统配置', '', 'layui-icon-set', 10, 'nav', 0, 1);
INSERT INTO `hq_store_auth` VALUES (14, 13, '账户配置', 'store/shop/index', '', 0, 'nav', 0, 1);
INSERT INTO `hq_store_auth` VALUES (15, 13, '字典管理', 'store/dict/index', '', 0, 'nav', 0, 1);
INSERT INTO `hq_store_auth` VALUES (16, 15, '添加', 'store/dict/add', '', 0, 'auth', 0, 1);
INSERT INTO `hq_store_auth` VALUES (17, 15, '编辑', 'store/dict/edit', '', 0, 'auth', 0, 1);
INSERT INTO `hq_store_auth` VALUES (18, 15, '删除', 'store/dict/del', '', 0, 'auth', 0, 1);
INSERT INTO `hq_store_auth` VALUES (109, 2, '角色管理', 'store/rule/index', '', 0, 'nav', 0, 1);
INSERT INTO `hq_store_auth` VALUES (110, 109, '添加', 'store/rule/add', '', 0, 'auth', 0, 1);
INSERT INTO `hq_store_auth` VALUES (111, 109, '编辑', 'store/rule/edit', '', 0, 'auth', 0, 1);
INSERT INTO `hq_store_auth` VALUES (112, 109, '删除', 'store/rule/del', '', 0, 'auth', 0, 1);
INSERT INTO `hq_store_auth` VALUES (164, 15, '设为默认', 'store/dict/edit_default', '', 0, 'auth', 0, 1);
INSERT INTO `hq_store_auth` VALUES (165, 13, '系统续费', 'store/index/renew', '', 0, 'nav', 0, 1);
INSERT INTO `hq_store_auth` VALUES (182, 0, '客户管理', '', 'layui-icon-user', 0, 'nav', 0, 1);
INSERT INTO `hq_store_auth` VALUES (183, 182, '客户列表', 'crm/customer/index', '', 0, 'nav', 0, 1);
INSERT INTO `hq_store_auth` VALUES (184, 183, '添加', 'crm/customer/add', '', 0, 'auth', 0, 1);
INSERT INTO `hq_store_auth` VALUES (185, 183, '编辑', 'crm/customer/edit', '', 0, 'auth', 0, 1);
INSERT INTO `hq_store_auth` VALUES (186, 183, '删除', 'crm/customer/del', '', 0, 'auth', 0, 1);
INSERT INTO `hq_store_auth` VALUES (187, 183, '转移', 'crm/customer/transfer', '', 0, 'auth', 0, 1);
INSERT INTO `hq_store_auth` VALUES (188, 182, '跟进记录', 'crm/track/index', '', 0, 'nav', 0, 1);
INSERT INTO `hq_store_auth` VALUES (189, 188, '添加', 'crm/track/add', '', 0, 'auth', 0, 1);
INSERT INTO `hq_store_auth` VALUES (190, 188, '编辑', 'crm/track/edit', '', 0, 'auth', 0, 1);
INSERT INTO `hq_store_auth` VALUES (191, 188, '删除', 'crm/track/del', '', 0, 'auth', 0, 1);
INSERT INTO `hq_store_auth` VALUES (192, 13, 'CRM字段', 'crm/setting/field', '', 0, 'nav', 0, 1);
INSERT INTO `hq_store_auth` VALUES (193, 183, '导入客户', 'crm/customer/import', '', 0, 'auth', 0, 1);
INSERT INTO `hq_store_auth` VALUES (194, 182, '合同管理', 'crm/contract/index', '', 0, 'nav', 0, 1);
INSERT INTO `hq_store_auth` VALUES (195, 194, '添加', 'crm/contract/add', '', 0, 'auth', 0, 1);
INSERT INTO `hq_store_auth` VALUES (196, 194, '编辑', 'crm/contract/edit', '', 0, 'auth', 0, 1);
INSERT INTO `hq_store_auth` VALUES (197, 194, '删除', 'crm/contract/del', '', 0, 'auth', 0, 1);
INSERT INTO `hq_store_auth` VALUES (198, 1, '我的工作台', 'store/index/home', '', 0, 'nav', 0, 1);
INSERT INTO `hq_store_auth` VALUES (199, 1, '消息中心', 'store/notice/index', '', 0, 'nav', 0, 1);
INSERT INTO `hq_store_auth` VALUES (200, 182, '公海客户', 'crm/customer/pool', '', 2, 'nav', 0, 1);
INSERT INTO `hq_store_auth` VALUES (201, 183, '领取', 'crm/customer/receive', '', 0, 'auth', 0, 1);
INSERT INTO `hq_store_auth` VALUES (203, 183, '详情', 'crm/customer/detail', '', 0, 'auth', 0, 1);
INSERT INTO `hq_store_auth` VALUES (204, 13, 'CRM配置', 'crm/setting/crm', '', 0, 'nav', 0, 1);
INSERT INTO `hq_store_auth` VALUES (209, 183, '转入公海', 'crm/customer/transfer_pool', '', 0, 'auth', 0, 1);
INSERT INTO `hq_store_auth` VALUES (210, 0, '微信互联', '', 'layui-icon-login-wechat', 0, 'nav', 0, 1);
INSERT INTO `hq_store_auth` VALUES (211, 210, '微信用户', 'wechat/user/index', '', 0, 'nav', 0, 1);
INSERT INTO `hq_store_auth` VALUES (212, 210, '自定义菜单', 'wechat/menu/index', '', 0, 'nav', 0, 1);
INSERT INTO `hq_store_auth` VALUES (213, 210, '模板消息', 'wechat/template/index', '', 0, 'nav', 0, 1);
INSERT INTO `hq_store_auth` VALUES (214, 210, '公众号配置', 'wechat/setting/index', '', 0, 'nav', 0, 1);
INSERT INTO `hq_store_auth` VALUES (215, 212, '添加菜单', 'wechat/menu/add', '', 0, 'auth', 0, 1);
INSERT INTO `hq_store_auth` VALUES (216, 212, '编辑', 'wechat/menu/edit', '', 0, 'auth', 0, 1);
INSERT INTO `hq_store_auth` VALUES (217, 212, '删除', 'wechat/menu/del', '', 0, 'auth', 0, 1);
INSERT INTO `hq_store_auth` VALUES (218, 213, '更新模板消息', 'wechat/template/update', '', 0, 'auth', 0, 1);
INSERT INTO `hq_store_auth` VALUES (219, 213, '编辑模板', 'wechat/template/edit', '', 0, 'auth', 0, 1);
INSERT INTO `hq_store_auth` VALUES (220, 213, '任务列表', 'wechat/template/task', '', 0, 'auth', 0, 1);
INSERT INTO `hq_store_auth` VALUES (221, 213, '添加发送任务', 'wechat/template/task_add', '', 0, 'auth', 0, 1);
INSERT INTO `hq_store_auth` VALUES (222, 213, '批量发送消息', 'wechat/template/task_send', '', 0, 'auth', 0, 1);

-- ----------------------------
-- Table structure for hq_store_group
-- ----------------------------
DROP TABLE IF EXISTS `hq_store_group`;
CREATE TABLE `hq_store_group`  (
  `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `store_id` smallint(5) NULL DEFAULT NULL COMMENT '商户ID',
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '部门名称',
  `pid` int(11) NULL DEFAULT 0 COMMENT '上级ID',
  `person` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '主管',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '部门介绍',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '状态',
  `sort_order` int(5) NOT NULL DEFAULT 0 COMMENT '排序',
  `create_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  `delete_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 225 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商户 部门表\r\n' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of hq_store_group
-- ----------------------------
INSERT INTO `hq_store_group` VALUES (215, 177, '总经理', 0, NULL, '', 1, 0, 1608815252, 1608867853, 0);
INSERT INTO `hq_store_group` VALUES (216, 177, '财务部', 215, NULL, '', 1, 0, 1608864027, 1608873120, 0);
INSERT INTO `hq_store_group` VALUES (217, 177, '技术部', 216, NULL, '', 1, 0, 1608864057, 1608867952, 0);
INSERT INTO `hq_store_group` VALUES (218, 177, '大老爷', 217, NULL, '', 1, 0, 1608864382, 1609307665, 0);
INSERT INTO `hq_store_group` VALUES (221, 177, '000', 216, NULL, '', 1, 0, 1608868872, 1608869052, 0);
INSERT INTO `hq_store_group` VALUES (222, 177, '恩恩', 215, NULL, '', 1, 0, 1608871647, 1608871667, 0);
INSERT INTO `hq_store_group` VALUES (223, 178, '总经理', 0, NULL, '', 1, 0, 1609126544, 1609126544, 0);
INSERT INTO `hq_store_group` VALUES (224, 178, '业务员', 223, NULL, '', 1, 0, 1609141325, 1609141325, 0);

-- ----------------------------
-- Table structure for hq_store_log
-- ----------------------------
DROP TABLE IF EXISTS `hq_store_log`;
CREATE TABLE `hq_store_log`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `store_id` smallint(5) NOT NULL COMMENT '商家ID',
  `user_id` smallint(5) UNSIGNED NOT NULL DEFAULT 0 COMMENT '管理员id',
  `client` smallint(3) NOT NULL DEFAULT 0 COMMENT '客户端',
  `ip` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'ip地址',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '请求链接',
  `method` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '请求类型',
  `type` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '资源类型',
  `param` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '请求参数',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '日志备注',
  `create_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(10) NULL DEFAULT NULL COMMENT '更新时间',
  `delete_time` int(10) NOT NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7946 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商户 操作日志' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of hq_store_log
-- ----------------------------


-- ----------------------------
-- Table structure for hq_store_rule
-- ----------------------------
DROP TABLE IF EXISTS `hq_store_rule`;
CREATE TABLE `hq_store_rule`  (
  `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `store_id` smallint(5) NULL DEFAULT NULL,
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `pid` int(11) NULL DEFAULT 0 COMMENT '上级',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `rules` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `sort_order` int(5) NOT NULL DEFAULT 0 COMMENT '排序',
  `create_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  `delete_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 219 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商户 用户角色' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of hq_store_rule
-- ----------------------------
INSERT INTO `hq_store_rule` VALUES (215, 177, '总经理', 0, '', 1, '1,198,199,182,183,184,185,186,187,193,201,203,209,188,189,190,191,194,195,196,197,200,210,211,212,213,214,2,3,6,7,8,4,9,10,11,109,110,111,112,5,12,13,14,15,16,17,18,164,165,192,204', 0, 1608815252, 1609307528, 0);
INSERT INTO `hq_store_rule` VALUES (216, 177, '副总', 0, '', 1, '1,198,199,182,183,184,185,186,203,188,189,190,191,194,195,196,197,200,210,211,212,213,214', 0, 1608879408, 1609145798, 0);
INSERT INTO `hq_store_rule` VALUES (217, 178, '总经理', 0, '', 1, '1,198,199,182,183,184,185,186,187,193,201,203,209,188,189,190,191,194,195,196,197,200,210,211,212,213,214,2,3,6,7,8,4,9,10,11,109,110,111,112,5,12,13,14,15,16,17,18,164,165,192,204', 0, 1609126544, 1609126544, 0);
INSERT INTO `hq_store_rule` VALUES (218, 178, '业务员', 0, '', 1, '1,198,199,182,183,184,185,186,187,193,201,203,209,188,189,190,191,194,195,196,197,200', 0, 1609141287, 1609141451, 0);

-- ----------------------------
-- Table structure for hq_system
-- ----------------------------
DROP TABLE IF EXISTS `hq_system`;
CREATE TABLE `hq_system`  (
  `id` int(3) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `key` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '配置字段',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '配置名称',
  `jdata` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '配置参数',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统配置表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of hq_system
-- ----------------------------
INSERT INTO `hq_system` VALUES (1, 'website', '网站配置', '{\"logo\":\"https:\\/\\/mdf.muxue.com.cn\\/uploads\\/20200813\\/9f5c379e923352382fd0de260868d262.jpg\",\"sitename\":\"\\u516d\\u8bfa\\u4e91\\u8425\\u9500\",\"title\":\"\\u516d\\u8bfa\\u4e91\\u8425\\u9500\",\"keywords\":\"131232\",\"description\":\"42\",\"copyright\":\"\\u516d\\u8bfa\\u79d1\\u6280\",\"beian\":\"\\u95fdICP\\u590718022286\\u53f7\"}');
INSERT INTO `hq_system` VALUES (2, 'qiniu', '七牛配置', '{\"model\":\"qiniu\",\"ak\":\"\",\"sk\":\"\",\"bucket\":\"\",\"domain\":\"\",\"type\":\"0\"}');
INSERT INTO `hq_system` VALUES (3, 'weixin', '公众号配置', '{\"model\":\"weixin\",\"appid\":\"\",\"appsecret\":\"\",\"token\":\"\",\"aes_key\":\"\",\"type\":\"0\"}');
INSERT INTO `hq_system` VALUES (4, 'wxapp', '小程序配置', '{\"model\":\"wxapp\",\"appid\":\"\",\"appsecret\":\"\",\"type\":\"0\"}');
INSERT INTO `hq_system` VALUES (5, 'wxpay', '微信支付配置', '{\"model\":\"wxpay\",\"appid\":\"\",\"mch_id\":\"\",\"key\":\"\",\"notify_url\":\"\",\"certpem\":\"\",\"keypem\":\"\",\"type\":\"0\"}');
INSERT INTO `hq_system` VALUES (6, 'qcloudsms', '腾讯云短信', '{\"model\":\"qcloudsms\",\"appid\":\"\",\"appkey\":\"\",\"type\":\"0\"}');
INSERT INTO `hq_system` VALUES (7, 'system', '参数配置', '{\"is_captcha\":\"0\"}');
INSERT INTO `hq_system` VALUES (10, 'upload_setting', '上传选项', NULL);

-- ----------------------------
-- Table structure for hq_taocan
-- ----------------------------
DROP TABLE IF EXISTS `hq_taocan`;
CREATE TABLE `hq_taocan`  (
  `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `rules` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `create_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  `delete_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '套餐' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of hq_taocan
-- ----------------------------
INSERT INTO `hq_taocan` VALUES (5, '默认套餐', '', 1, '1,198,199,182,183,184,185,186,187,193,201,203,209,188,189,190,191,194,195,196,197,200,210,211,212,213,214,2,3,6,7,8,4,9,10,11,109,110,111,112,5,12,13,14,15,16,17,18,164,165,192,204', 1594197300, 1609055420, 0);

-- ----------------------------
-- Table structure for hq_uploads
-- ----------------------------
DROP TABLE IF EXISTS `hq_uploads`;
CREATE TABLE `hq_uploads`  (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `store_id` int(5) NULL DEFAULT 0 COMMENT '商户ID',
  `user_id` int(5) UNSIGNED NULL DEFAULT 0 COMMENT '用户ID',
  `storage` char(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '存储位置',
  `file_url` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '存储域名',
  `file_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件名',
  `file_size` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件大小',
  `mine` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` int(3) NULL DEFAULT 1 COMMENT '状态',
  `create_time` int(10) NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(10) NOT NULL DEFAULT 0 COMMENT '更新时间',
  `delete_time` int(10) NOT NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 213 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '附件表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of hq_uploads
-- ----------------------------

-- ----------------------------
-- Table structure for hq_user
-- ----------------------------
DROP TABLE IF EXISTS `hq_user`;
CREATE TABLE `hq_user`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `store_id` int(8) NOT NULL DEFAULT 0 COMMENT '商户ID',
  `mobile` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '手机',
  `password` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '密码',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `avatarurl` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '头像',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '0禁用/1启动',
  `openid` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '小程序appid',
  `last_login_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '上次登录时间',
  `last_login_ip` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '上次登录IP',
  `login_count` int(11) NOT NULL DEFAULT 0 COMMENT '登录次数',
  `sort_order` smallint(5) NULL DEFAULT 0 COMMENT '排序',
  `group_id` int(3) NULL DEFAULT NULL COMMENT '所属部门',
  `rule_id` char(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '角色管理',
  `create_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  `delete_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 298 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '商户 用户表\r\n' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of hq_user
-- ----------------------------
INSERT INTO `hq_user` VALUES (293, 177, '13459401111', 'e10adc3949ba59abbe56e057f20f883e', '黄青', NULL, 1, NULL, 1610077775, '183.199.172.8', 70, 0, 215, '216', 1608815252, 1610077775, 0);
INSERT INTO `hq_user` VALUES (294, 177, '13055556455', 'e10adc3949ba59abbe56e057f20f883e', '黄佳慧', NULL, 1, NULL, 1610083525, '111.196.146.217', 38, 0, 216, '215', 1608864778, 1610083525, 0);

-- ----------------------------
-- Table structure for hq_wechat
-- ----------------------------
DROP TABLE IF EXISTS `hq_wechat`;
CREATE TABLE `hq_wechat`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `mobile` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '手机号码',
  `openid` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'OPENID',
  `nickname` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '昵称',
  `gender` tinyint(5) NULL DEFAULT NULL COMMENT '性别',
  `city` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '城市',
  `province` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '省',
  `country` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '国家',
  `avatarurl` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '头像',
  `unionid` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'UNIONID',
  `status` tinyint(5) NOT NULL DEFAULT 1 COMMENT '状态 0禁用 1启用',
  `store_id` int(5) NOT NULL DEFAULT 0 COMMENT '默认商户 0无商户',
  `user_id` int(5) NOT NULL DEFAULT 0 COMMENT '绑定用户',
  `create_time` int(11) NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(11) NOT NULL DEFAULT 0 COMMENT '更新时间',
  `delete_time` int(11) NOT NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 51 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of hq_wechat
-- ----------------------------

-- ----------------------------
-- Table structure for hq_wechat_menu
-- ----------------------------
DROP TABLE IF EXISTS `hq_wechat_menu`;
CREATE TABLE `hq_wechat_menu`  (
  `id` int(5) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `store_id` int(5) NOT NULL DEFAULT 0 COMMENT '商户ID',
  `pid` smallint(5) NULL DEFAULT 0 COMMENT '按钮级别 0 button 1 sub_button',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单标题',
  `sub_button` smallint(2) NULL DEFAULT 0 COMMENT '菜单类型 0无子菜单 1有子菜单',
  `type` char(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单的响应动作类型，view表示网页类型，click表示点击类型，miniprogram表示小程序类型',
  `value` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单Value',
  `key` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单KEY值',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '网页 链接',
  `media_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '素材ID',
  `appid` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '小程序APPID',
  `pagepath` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '小程序路径',
  `sort_order` smallint(3) NULL DEFAULT 0 COMMENT '排序',
  `create_time` int(10) NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(10) NOT NULL DEFAULT 0 COMMENT '更新时间',
  `delete_time` int(10) NOT NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 402 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of hq_wechat_menu
-- ----------------------------

-- ----------------------------
-- Table structure for hq_wechat_news
-- ----------------------------
DROP TABLE IF EXISTS `hq_wechat_news`;
CREATE TABLE `hq_wechat_news`  (
  `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `store_id` int(5) NULL DEFAULT NULL COMMENT '商户ID',
  `media_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'MEDIAID',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
  `thumb_media_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图文消息的封面',
  `show_cover_pic` int(2) NULL DEFAULT NULL COMMENT '是否显示封面0不显示 1显示',
  `author` char(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '作者',
  `digest` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '摘要',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '详情',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图文URL',
  `content_source_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '阅读原文',
  `thumb_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '缩略图地址',
  `need_open_comment` int(5) NULL DEFAULT NULL,
  `only_fans_can_comment` int(5) NULL DEFAULT NULL,
  `sort_order` int(3) NULL DEFAULT 0 COMMENT '序列',
  `create_time` int(10) NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `delete_time` int(10) NOT NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of hq_wechat_news
-- ----------------------------

-- ----------------------------
-- Table structure for hq_wechat_reply
-- ----------------------------
DROP TABLE IF EXISTS `hq_wechat_reply`;
CREATE TABLE `hq_wechat_reply`  (
  `id` int(5) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `store_id` int(5) NULL DEFAULT NULL COMMENT '商户ID',
  `keyword` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '关键字',
  `type` char(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类型',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '内容',
  `mediaid` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'MediaId',
  `status` int(2) NULL DEFAULT 1 COMMENT '状态',
  `create_time` int(10) NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(10) NOT NULL DEFAULT 0 COMMENT '更新时间',
  `delete_time` int(10) NOT NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of hq_wechat_reply
-- ----------------------------

-- ----------------------------
-- Table structure for hq_wechat_template
-- ----------------------------
DROP TABLE IF EXISTS `hq_wechat_template`;
CREATE TABLE `hq_wechat_template`  (
  `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `store_id` int(5) NOT NULL DEFAULT 0 COMMENT '商户ID',
  `template_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '模板ID',
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '模板标题',
  `primary_industry` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '模板大类',
  `deputy_industry` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '模板小类',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '模板内容',
  `example` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '模板范例',
  `url` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '跳转URL',
  `appid` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'APPID',
  `pagepath` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '跳转链接',
  `jdata` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '发送数据',
  `create_time` int(11) NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(11) NOT NULL DEFAULT 0 COMMENT '更新时间',
  `delete_time` int(11) NOT NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 205 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of hq_wechat_template
-- ----------------------------

-- ----------------------------
-- Table structure for hq_wechat_template_task
-- ----------------------------
DROP TABLE IF EXISTS `hq_wechat_template_task`;
CREATE TABLE `hq_wechat_template_task`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `store_id` int(10) NOT NULL DEFAULT 0 COMMENT '商户ID',
  `user_id` int(10) NOT NULL DEFAULT 0 COMMENT '用户ID',
  `tid` int(10) NOT NULL DEFAULT 0 COMMENT '任务ID',
  `status` int(5) NOT NULL DEFAULT 0 COMMENT '状态 0待发 1已发',
  `error` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '错误提示',
  `create_time` int(11) NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(11) NOT NULL DEFAULT 0 COMMENT '更新时间',
  `delete_time` int(11) NOT NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '微信 模板消息任务表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of hq_wechat_template_task
-- ----------------------------

-- ----------------------------
-- Table structure for hq_wechat_user
-- ----------------------------
DROP TABLE IF EXISTS `hq_wechat_user`;
CREATE TABLE `hq_wechat_user`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `store_id` int(5) NULL DEFAULT 0 COMMENT '商户ID',
  `nickname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '昵称',
  `gender` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '性别',
  `country` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '国家',
  `province` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '省',
  `city` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '市',
  `avatarurl` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '头像',
  `unionid` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'UNIONID',
  `openid` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'OPENID',
  `sort_order` smallint(5) NULL DEFAULT 0 COMMENT '排序',
  `language` char(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '语言',
  `subscribe` char(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '关注与否',
  `subscribe_time` int(10) NULL DEFAULT 0 COMMENT '关注时间',
  `subscribe_scene` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '关注渠道',
  `create_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  `delete_time` int(10) NOT NULL DEFAULT 0 COMMENT '删除时间',
  `is_getinfo` int(5) NOT NULL DEFAULT 0 COMMENT '是否获取信息 0否 1是',
  `error` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '错误',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `id`(`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 225085 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '会员' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of hq_wechat_user
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
