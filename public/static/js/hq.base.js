var layer = layui.layer,
    form = layui.form,
    element = layui.element,
    laydate = layui.laydate,
    upload = layui.upload,
    table = layui.table;

// 通用提交
form.on('submit(*)', function (data) {
    var index = layer.msg('提交中，请稍候', {
        icon: 16,
        time: false,
        shade: 0.3
    });
    $(data.elem).attr('disabled', true);
    $.ajax({
        url: data.form.action,
        type: data.form.method,
        dataType: 'json',
        data: $(data.form).serialize(),
        success: function (result) {
            if (result.code === 1 && result.url != '') {
                setTimeout(function () {
                    location.href = result.url;
                }, 1000);
            } else {
                $(data.elem).attr('disabled', false);
            }
            layer.close(index);
            layer.msg(result.msg, {icon: 1});
        },
        error: function (xhr, state, errorThrown) {
            layer.close(index);
            layer.msg(state + '：' + errorThrown, {icon: 2});
        }
    });
    return false;
});
// 父窗口通用提交
form.on('submit(i)', function (data) {
    var index = layer.msg('提交中，请稍候', {
        icon: 16,
        time: false,
        shade: 0.3
    });
    $.ajax({
        url: data.form.action,
        type: data.form.method,
        dataType: 'json',
        data: $(data.form).serialize(),
        success: function (result) {
            if (result.code === 1) {
                setTimeout(function () {
                    parent.location.reload();
                }, 1000);
            }
            layer.close(index);
            layer.msg(result.msg, {icon: 1});
        },
        error: function (xhr, state, errorThrown) {
            layer.close(index);
            layer.msg(state + '：' + errorThrown, {icon: 2});
        }
    });
    return false;
});
// 通用开关
form.on('switch(*)', function (data) {
    var index = layer.msg('修改中，请稍候', {
        icon: 16,
        time: false,
        shade: 0.3
    });
    // 参数
    var obj = {};
    obj[$(this).attr('name')] = this.checked == true ? 1 : 0;
    obj['_verify'] = 0;
    $.ajax({
        url: $(this).data('url'),
        type: 'post',
        dataType: 'json',
        data: obj,
        success: function (result) {
            layer.close(index);
            layer.msg(result.msg, {icon: 1});
        },
        error: function (xhr, state, errorThrown) {
            layer.close(index);
            layer.msg(state + '：' + errorThrown, {icon: 2});
        }
    });
});
// 通用全选
form.on('checkbox(*)', function (data) {
    $('.layui-table tbody input[lay-skin="primary"]').each(function (index, item) {
        item.checked = data.elem.checked;
    });
    form.render('checkbox');
});
// 通用提交
$('.ajax-submit').on('click', function () {
    var than = $(this);
    var form = $(this).parents('form');
    var index = layer.msg('提交中，请稍候', {
        icon: 16,
        time: false,
        shade: 0.3
    });
    than.attr('disabled', true);
    $.ajax({
        url: form.attr('action'),
        type: form.attr('method'),
        dataType: 'json',
        data: $(data.form).serialize(),
        success: function (result) {
            if (result.code === 1 && result.url != '') {
                setTimeout(function () {
                    location.href = result.url;
                }, 1000);
            } else {
                than.attr('disabled', false);
            }
            layer.close(index);
            layer.msg(result.msg, {icon: 1});
        },
        error: function (xhr, state, errorThrown) {
            layer.close(index);
            layer.msg(state + '：' + errorThrown, {icon: 2});
        }
    });
    return false;
});
// 通用异步
$('.ajax-action').on('click', function () {
    var url = $(this).attr('href');
    var index = layer.msg('请求中，请稍候', {
        icon: 16,
        time: false,
        shade: 0.3
    });
    $.ajax({
        url: url,
        type: 'post',
        dataType: 'json',
        success: function (result) {
            if (result.code === 1 && result.url != '') {
                setTimeout(function () {
                    location.href = result.url;
                }, 1000);
            }
            layer.close(index);
            layer.msg(result.msg, {icon: 1});
        },
        error: function (xhr, state, errorThrown) {
            layer.close(index);
            layer.msg(state + '：' + errorThrown, {icon: 2});
        }
    });
    return false;
});
// 通用异步
// 数据表格专用
$('body').on('click','.ajax-action',function(){
    var url = $(this).attr('href');
    var index = layer.msg('请求中，请稍候', {
        icon: 16,
        time: false,
        shade: 0.3
    });
    $.ajax({
        url: url,
        type: 'post',
        dataType: 'json',
        success: function (result) {
            if (result.code === 1 && result.url != '') {
                setTimeout(function () {
                    location.href = result.url;
                }, 1000);
            }
            layer.close(index);
            layer.msg(result.msg, {icon: 1});
        },
        error: function (xhr, state, errorThrown) {
            layer.close(index);
            layer.msg(state + '：' + errorThrown, {icon: 2});
        }
    });
    return false;
});
// 通用更新
$('.ajax-update').on('blur', function () {
    // 参数
    var obj = {};
    obj[$(this).attr('name')] = $(this).val();
    obj['_verify'] = 0;
    var index = layer.msg('请求中，请稍候', {
        icon: 16,
        time: false,
        shade: 0.3
    });
    $.ajax({
        url: $(this).data('url'),
        type: 'post',
        dataType: 'json',
        data: obj,
        success: function (result) {
            if (result.code === 1) {
                layer.msg(result.msg, {icon: 1});
                setTimeout(function () {
                    location.reload();
                }, 1000);
            }
        },
        error: function (xhr, state, errorThrown) {
            layer.close(index);
            layer.msg(state + '：' + errorThrown, {icon: 2});
        }
    });
    return false;
});
// 数据表格专用
$('body').on('blur','.ajax-update',function(){
    // 参数
    var obj = {};
    obj[$(this).attr('name')] = $(this).val();
    obj['_verify'] = 0;
    var index = layer.msg('请求中，请稍候', {
        icon: 16,
        time: false,
        shade: 0.3
    });
    $.ajax({
        url: $(this).data('url'),
        type: 'post',
        dataType: 'json',
        data: obj,
        success: function (result) {
            if (result.code === 1) {
                layer.msg(result.msg, {icon: 1});
                setTimeout(function () {
                    location.reload();
                }, 1000);
            }
        },
        error: function (xhr, state, errorThrown) {
            layer.close(index);
            layer.msg(state + '：' + errorThrown, {icon: 2});
        }
    });
    return false;
});
// 通用删除
$('.ajax-delete').on('click', function () {
    var url = $(this).attr('href');
    layer.confirm('确定操作？', {
        icon: 3,
        title: '提示'
    }, function (index) {
        var index = layer.msg('操作中，请稍候', {
            icon: 16,
            time: false,
            shade: 0.3
        });
        $.ajax({
            url: url,
            type: 'post',
            dataType: 'json',
            success: function (result) {
                if (result.code === 1 && result.url != '') {
                    setTimeout(function () {
                        location.href = result.url;
                    }, 1000);
                }
                layer.close(index);
                layer.msg(result.msg, {icon: 1});
            },
            error: function (xhr, state, errorThrown) {
                layer.close(index);
                layer.msg(state + '：' + errorThrown, {icon: 2});
            }
        });
    });
    return false;
});
//数据表格专用
$('body').on('click','.ajax-delete',function(){
    var url = $(this).attr('href');
    layer.confirm('确定操作？', {
        icon: 3,
        title: '提示'
    }, function (index) {
        var index = layer.msg('操作中，请稍候', {
            icon: 16,
            time: false,
            shade: 0.3
        });
        $.ajax({
            url: url,
            type: 'post',
            dataType: 'json',
            success: function (result) {
                if (result.code === 1 && result.url != '') {
                    setTimeout(function () {
                        location.href = result.url;
                    }, 1000);
                }
                layer.close(index);
                layer.msg(result.msg, {icon: 1});
            },
            error: function (xhr, state, errorThrown) {
                layer.close(index);
                layer.msg(state + '：' + errorThrown, {icon: 2});
            }
        });
    });
    return false;
});
//数据表格专用
$('body').on('click','.ajax-detail',function(){
    var title = $(this).html();
    var url = $(this).attr('href');
    var index = layer.open({
        title: title,
        type: 2,
        content: url,
        success: function (layero, index) {
            setTimeout(function () {
                layer.tips('点击此处返回', '.layui-layer-setwin .layui-layer-close', {
                    tips: 3
                });
            }, 500)
        }
    })
    layer.full(index);
    return false;
});
// 通用详情
$('.ajax-detail').on('click', function () {
    var title = $(this).html();
    var url = $(this).attr('href');
    var index = layer.open({
        title: title,
        type: 2,
        content: url,
        success: function (layero, index) {
            setTimeout(function () {
                layer.tips('点击此处返回', '.layui-layer-setwin .layui-layer-close', {
                    tips: 3
                });
            }, 500)
        }
    })
    layer.full(index);
    return false;
});
// 通用窗口
$('.ajax-iframe').on('click', function() {
    var title = $(this).html();
    var url = $(this).attr('href');
    var width = $(this).data('width');
    var height = $(this).data('height');
    if(navigator.userAgent.match(/(iPhone|iPod|Android|ios)/i)){
        var width = '100%';
        var height = '100%';
    };
    var index = layer.open({
        title: title,
        type: 2,
        anim: 0,
        shadeClose:true,
        area: [width, height],
        content: url,
        success: function(layero, index) {
            //获取IFRAME高度
            var iframeheight = layui.$(window).height();
            //获取浏览器高度
            var windowheight = layer.getChildFrame('html',index).outerHeight();
            //如果浏览器高度大于或者等于IFRAME
            if(windowheight <= iframeheight){
                if(navigator.userAgent.match(/(iPhone|iPod|Android|ios)/i)){
                    
                }else{
                    layer.iframeAuto(index);
                    layer.style(index, {
                        // 重新居中的样式
                        top: (layui.$(window).height() - layui.$(layero).height()) / 2
                    })
                }
            }
        },
    })
    return false;
});
//数据表格专用
$('body').on('click','.ajax-iframe',function(){
    var title = $(this).html();
    var url = $(this).attr('href');
    var width = $(this).data('width');
    var height = $(this).data('height');
    if(navigator.userAgent.match(/(iPhone|iPod|Android|ios)/i)){
        var width = '100%';
        var height = '100%';
    };
    var index = layer.open({
        title: title,
        type: 2,
        anim: 0,
        shadeClose:true,
        area: [width, height],
        content: url,
        success: function(layero, index) {
            //获取IFRAME高度
            var iframeheight = layui.$(window).height();
            //获取浏览器高度
            var windowheight = layer.getChildFrame('html',index).outerHeight();
            //如果浏览器高度大于或者等于IFRAME
            if(windowheight <= iframeheight){
                if(navigator.userAgent.match(/(iPhone|iPod|Android|ios)/i)){
                    
                }else{
                    layer.iframeAuto(index);
                    layer.style(index, {
                        // 重新居中的样式
                        top: (layui.$(window).height() - layui.$(layero).height()) / 2
                    })
                }
            }
        },
    })
    return false;
});
// 通用搜索
$('.ajax-search').on('click', function () {
    var form = $(this).parents('form');
    var url = form.attr('action');
    var query = form.serialize();
    query = query.replace(/(&|^)(\w*?\d*?\-*?_*?)*?=?((?=&)|(?=$))/g, '');
    query = query.replace(/^&/g, '');
    if (url.indexOf('?') > 0) {
        url += '&' + query;
    } else {
        url += '?' + query;
    }
    location.href = url;
    return false;
});
/* 表格搜索 */
form.on('submit(ListSearch)', function (data) {
    insTb.reload({where: data.field, page: {curr: 1}});
    return false;
});
// 通用批量
$('.ajax-batch').on('click', function () {
    var url = $(this).attr('href');
    var val = [];
    $('.layui-table tbody input[lay-skin="primary"]:checked').each(function (i) {
        val[i] = $(this).val();
    });
    if (val === undefined || val.length == 0) {
        layer.msg('请选择数据');
        return false;
    }
    var index = layer.msg('请求中，请稍候', {
        icon: 16,
        time: false,
        shade: 0.3
    });
    // 参数
    var obj = {};
    obj[$('.layui-table tbody input[lay-skin="primary"]:checked').attr('name')] = val;
    obj['_verify'] = 0;
    $.ajax({
        url: url,
        type: 'post',
        dataType: 'json',
        data: obj,
        success: function (result) {
            if (result.code === 1 && result.url != '') {
                setTimeout(function () {
                    location.reload();
                }, 1000);
            }
            layer.close(index);
            layer.msg(result.msg, {icon: 1});
        },
        error: function (xhr, state, errorThrown) {
            layer.close(index);
            layer.msg(state + '：' + errorThrown, {icon: 2});
        }
    });
    return false;
});
// 头像上传
upload.render({
    elem: '.ajax-avatar',
    url: '/store/uploads/uploadImage',
    done: function (result) {
        var avatar_id = this.item[0].dataset.id;
        console.log(this.item[0])
        // 上传完毕回调
        if (result.code === 1) {            
            $('#' + avatar_id + '-input').val(result.url);
            $('#' + avatar_id + '-img').attr('src', result.url);
            layer.msg('上传成功');
        } else {
            layer.msg(result.msg);
        }
    }
});
// 通用上传
upload.render({
    elem: '.ajax-images',
    url: '/store/uploads/uploadImage',
    done: function (result) {
        // 上传完毕回调
        if (result.code === 1) {
            this.item.prev('input').val(result.url);
        } else {
            layer.msg(result.msg, {icon: 2});
        }
    }
});
upload.render({
    elem: '.ajax-file',
    url: '/store/uploads/uploadFile',
    accept: 'file', // 普通文件
    exts:'pdf|doc|docx|xls|xlsx|png|jpg|jpeg|gif|bmp',
    done: function (result) {
        // 上传完毕回调
        if (result.code === 1) {
            this.item.prev('input').val(result.url);
        } else {
            layer.msg(result.msg, {icon: 2});
        }
    }
});
upload.render({
    elem: '.ajax-video',
    url: '/store/uploads/uploadVideo',
    accept: 'video', // 视频文件
    done: function (result) {
        // 上传完毕回调
        if (result.code === 1) {
            this.item.prev('input').val(result.url);
        } else {
            layer.msg(result.msg, {icon: 2});
        }
    }
});
// 通用相册
upload.render({
    elem: '.ajax-photos',
    url: '/store/uploads/uploadImage',
    size: 1024,
    exts: 'jpg|png|jpeg',
    multiple: true,
    before: function(obj) {
        layer.msg('图片上传中...', {
            icon: 16,
            shade: 0.01,
            time: 0
        })
    },
    done: function(res,index,upload) {
        layer.close(layer.msg());//关闭上传提示窗口
        if(res.status == 0) {
            return layer.msg(res.msg);
        }
        $('#slide-pc-priview').append('<li style="position:relative"><img src="' + res.url + '" width="120" height="120"><div class="close img_close">X</div><input type="hidden" name="photo[]" value="' + res.url + '" /></li>');
    }
});
// 删除相册
$('.layui-form').delegate('.delete-photo', 'click', function () {
    $(this).parents('.layui-form-item').remove();
});

// 点击图片放大
$(document).off('click.tbImg').on('click.tbImg', '[tb-img]', function () {
    layer.photos({photos: {data: [{src: $(this).attr('src')}]}, shade: .1, closeBtn: true});
});

// 以下 管理员端
// 头像上传
upload.render({
    elem: '.ajax-admin-avatar',
    url: '/lnadmin/uploads/uploadImage',
    done: function (result) {
        var avatar_id = this.item[0].dataset.id;
        console.log(this.item[0])
        // 上传完毕回调
        if (result.code === 1) {            
            $('#' + avatar_id + '-input').val(result.url);
            $('#' + avatar_id + '-img').attr('src', result.url);
            layer.msg('上传成功');
        } else {
            layer.msg(result.msg);
        }
    }
});
// 通用上传
upload.render({
    elem: '.ajax-admin-images',
    url: '/lnadmin/uploads/uploadImage',
    done: function (result) {
        // 上传完毕回调
        if (result.code === 1) {
            this.item.prev('input').val(result.url);
        } else {
            layer.msg(result.msg, {icon: 2});
        }
    }
});
upload.render({
    elem: '.ajax-admin-file',
    url: '/lnadmin/uploads/uploadFile',
    accept: 'file', // 普通文件
    exts:'pdf|doc|docx|xls|xlsx|png|jpg|jpeg|gif|bmp',
    done: function (result) {
        // 上传完毕回调
        if (result.code === 1) {
            this.item.prev('input').val(result.url);
        } else {
            layer.msg(result.msg, {icon: 2});
        }
    }
});
upload.render({
    elem: '.ajax-admin-video',
    url: '/lnadmin/uploads/uploadVideo',
    accept: 'video', // 视频文件
    exts:'mp3|mp4',
    done: function (result) {
        // 上传完毕回调
        if (result.code === 1) {
            this.item.prev('input').val(result.url);
        } else {
            layer.msg(result.msg, {icon: 2});
        }
    }
});
// 通用相册
upload.render({
    elem: '.ajax-admin-photos',
    url: '/lnadmin/uploads/uploadImage',
    size: 1024,
    exts: 'jpg|png|jpeg',
    multiple: true,
    before: function(obj) {
        layer.msg('图片上传中...', {
           icon: 16,
           shade: 0.01,
           time: 0
        })
    },
    done: function(res,index,upload) {
        layer.close(layer.msg());//关闭上传提示窗口
        if(res.status == 0) {
            return layer.msg(res.msg);
        }
        $('#slide-pc-priview').append('<li style="position:relative"><img src="' + res.url + '" width="120" height="120"><div class="close img_close">X</div><input type="hidden" name="photo[]" value="' + res.url + '" /></li>');
    }
});
//点击多图上传的X,删除当前的图片   
$("body").on("click",".close",function(){
    $(this).closest("li").remove();
});
//多图上传排序
$("body").on("click",".mitpic",function(){
    $(this).dragsort({
        dragSelector: "img",
        dragBetween: true ,
        dragEnd:function(){

        }
    })
});
