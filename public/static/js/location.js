//调用方法
var geocoder,map, marker = null;

var init = function(){
    var latitude = document.getElementById('location').getAttribute('data-lat');
    var longitude = document.getElementById('location').getAttribute('data-lng');
    console.log(latitude+'/'+longitude)
    if(longitude != '' && latitude != ''){
        var lat= latitude;
        var lng = longitude;
    }else{
        //获取城市列表接口设置中心点
        citylocation = new qq.maps.CityService({
            complete : function(result){
                map.setCenter(result.detail.latLng);
            }
        });
        //调用searchLocalCity();方法    根据用户IP查询城市信息。
        citylocation.searchLocalCity();

        var lat= 23.148963;
        var lng = 113.257788;
    }

    var center = new qq.maps.LatLng(lat,lng);
    var map = new qq.maps.Map(document.getElementById("container"),{
        center: center,
        zoom: 16
    });

    //Marker覆盖物
    var anchor = new qq.maps.Point(6, 6),
        size = new qq.maps.Size(64, 64),
        origin = new qq.maps.Point(0, 0),
        icon = new qq.maps.MarkerImage('https://lbs.qq.com/doc_v2/img/nilt.png', size, origin, anchor); //引用自己定义的图标
    var marker = new qq.maps.Marker({
        position:center,
        animation:qq.maps.MarkerAnimation.DROP,
        map:map,
        icon:icon,
    });

    //实例化自动完成
    var ap = new qq.maps.place.Autocomplete(document.getElementById('place'));
    //调用Poi检索类。用于进行本地检索、周边检索等服务。
    var searchService = new qq.maps.SearchService({
        map : map
    });
    //添加监听事件
    qq.maps.event.addListener(ap, "confirm", function(res){
        searchService.search(res.value);
    });

    //添加监听事件   获取鼠标单击事件
    qq.maps.event.addListener(map, 'click', function(event) {
        console.log(event,'测试测试');
        //Marker覆盖物
        var marker=new qq.maps.Marker({
            position:event.latLng,
            map:map
        });
        qq.maps.event.addListener(map, 'click', function(event) {
            marker.setMap(null);
        });

        var jw = event.latLng;
        var latLng = new qq.maps.LatLng(jw["lat"], jw["lng"]);
        //调用信息窗口
        var info = new qq.maps.InfoWindow({map: map});
        //调用获取位置方法
        geocoder.getAddress(latLng);

    });

    var info = new qq.maps.InfoWindow({map: map});
    geocoder = new qq.maps.Geocoder({
        complete : function(result){
            console.log(result,'地址');
            // var b = result.detail.addressComponents;
            //b.province+b.city+b.district+b.town+b.street+
            var b = result.detail;
            console.log(b.nearPois[0].address,'-*-*-*-',b.nearPois[0].name);
            $('#place').val(b.nearPois[0].address+b.nearPois[0].name);
            var lat = b.location.lat;
            var lng = b.location.lng;
            $('#latitude').val(lat);
            $('#longitude').val(lng);
        }
    });
}


